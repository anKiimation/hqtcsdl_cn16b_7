﻿using Cua_Hang_An_Vat.Folder_Controller;
using Cua_Hang_An_Vat.FolderForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.GUI
{
    public partial class FormMenu : Form
    {
        MonAnController monAnController;

        public FormMenu()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void khoiTao()
        {
            Program.khoiTaoControl(this);

        }

        private void FormMenu_Load(object sender, EventArgs e)
        {
            khoiTao();
           monAnController  = new MonAnController(dataMonAn, txtMaSo, txtTenMonAn,  txtGiaTien, comboKhaDung,txtTimKiem);
        }

        public void refresh()
        {
            monAnController.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormThemMonAn formThemMonAn = new FormThemMonAn();
            formThemMonAn.FormMenu = this;
            formThemMonAn.Show();
        }

   

        private void dataMonAn_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            monAnController.hienThiThongTinKhiChonVaoDataGridView(e.RowIndex);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            monAnController.xoaMonAn();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            monAnController.capNhatMonAn();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            monAnController.timKiemMonAn();
        }

        private void FormMenu_Click(object sender, EventArgs e)
        {
            monAnController.unselect();
        }

        private void dataMonAn_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtGiaTien_TextChanged(object sender, EventArgs e)
        {
            Program.tienTe(txtGiaTien);
        }

        
    }
}
