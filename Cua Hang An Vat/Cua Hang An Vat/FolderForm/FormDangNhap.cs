﻿using Cua_Hang_An_Vat.Folder_Class;
using Cua_Hang_An_Vat.Folder_Controller;
using Cua_Hang_An_Vat.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.FolderForm
{
    public partial class FormDangNhap : Form
    {

        DangNhapController dangNhapController;

        public FormDangNhap()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Main main = new Main();
            TaiKhoan taiKhoan = dangNhapController.dangNhap();
            if (taiKhoan != null)
            {
                main.TaiKhoan = taiKhoan;

                main.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Sai thông tin đăng nhập!", "Đăng nhập thất bại");
            }
            
        }
        private void khoiTao()
        {
            Program.khoiTaoControl(this);

        }

        private void FormDangNhap_Load(object sender, EventArgs e)
        {
            khoiTao();
            dangNhapController = new DangNhapController(txtTaiKhoan, txtMatKhau);
        }

       
    }
}
