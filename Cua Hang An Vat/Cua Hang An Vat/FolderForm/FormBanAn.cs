﻿using Cua_Hang_An_Vat.BUS;
using Cua_Hang_An_Vat.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.FolderForm
{
    public partial class FormBanAn : Form
    {
        Main mainForm;

        public Main MainForm
        {
            get { return mainForm; }
            set { mainForm = value; }
        }
        BanAnController banAnController;
        public FormBanAn()
        {
            InitializeComponent();
        }
        public void khoiTao()
        {
            Program.khoiTaoControl(this);
        }

        private void FormBanAn_Load(object sender, EventArgs e)
        {
            khoiTao();
            banAnController = new BanAnController(panelQuanLyBanAn,btnAnHien);

            //
            
        }

        private void btnAnHien_Click(object sender, EventArgs e)
        {
            
            banAnController.anHienBanAn();
        }

        private void btnThemBanAn_Click(object sender, EventArgs e)
        {
            banAnController.themBanAn();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            mainForm.loadBanAn();
            this.Close();
        }
    }
}
