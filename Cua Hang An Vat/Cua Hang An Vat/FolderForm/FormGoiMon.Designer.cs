﻿namespace Cua_Hang_An_Vat.GUI
{
    partial class FormGoiMon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTimKiem = new System.Windows.Forms.TextBox();
            this.dataMonAn = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataMonGoi = new System.Windows.Forms.DataGridView();
            this.columnMaSo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnTenMonAn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnGia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSoLuong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnThemMonAn = new System.Windows.Forms.Button();
            this.btnXoaMonAn = new System.Windows.Forms.Button();
            this.btnGoiMon = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.txtMaBanAn = new System.Windows.Forms.TextBox();
            this.txtThoiGianVaoBan = new System.Windows.Forms.TextBox();
            this.txtSoLuongThem = new System.Windows.Forms.TextBox();
            this.txtSoLuongGiam = new System.Windows.Forms.TextBox();
            this.txtTenMonAn = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataMonAn)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataMonGoi)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtTimKiem);
            this.groupBox1.Controls.Add(this.dataMonAn);
            this.groupBox1.Location = new System.Drawing.Point(11, 12);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(299, 466);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thực đơn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tìm kiếm:";
            // 
            // txtTimKiem
            // 
            this.txtTimKiem.Location = new System.Drawing.Point(64, 17);
            this.txtTimKiem.Margin = new System.Windows.Forms.Padding(2);
            this.txtTimKiem.Name = "txtTimKiem";
            this.txtTimKiem.Size = new System.Drawing.Size(231, 20);
            this.txtTimKiem.TabIndex = 1;
            this.txtTimKiem.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // dataMonAn
            // 
            this.dataMonAn.AllowUserToAddRows = false;
            this.dataMonAn.AllowUserToDeleteRows = false;
            this.dataMonAn.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataMonAn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataMonAn.Location = new System.Drawing.Point(5, 40);
            this.dataMonAn.Margin = new System.Windows.Forms.Padding(2);
            this.dataMonAn.Name = "dataMonAn";
            this.dataMonAn.ReadOnly = true;
            this.dataMonAn.RowTemplate.Height = 24;
            this.dataMonAn.Size = new System.Drawing.Size(290, 421);
            this.dataMonAn.TabIndex = 0;
            this.dataMonAn.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataMonAn_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataMonGoi);
            this.groupBox2.Location = new System.Drawing.Point(529, 12);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(288, 466);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách món đã chọn";
            // 
            // dataMonGoi
            // 
            this.dataMonGoi.AllowUserToAddRows = false;
            this.dataMonGoi.AllowUserToDeleteRows = false;
            this.dataMonGoi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataMonGoi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataMonGoi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnMaSo,
            this.columnTenMonAn,
            this.columnGia,
            this.columnSoLuong});
            this.dataMonGoi.Location = new System.Drawing.Point(6, 17);
            this.dataMonGoi.Margin = new System.Windows.Forms.Padding(2);
            this.dataMonGoi.Name = "dataMonGoi";
            this.dataMonGoi.ReadOnly = true;
            this.dataMonGoi.RowTemplate.Height = 24;
            this.dataMonGoi.Size = new System.Drawing.Size(278, 443);
            this.dataMonGoi.TabIndex = 1;
            this.dataMonGoi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataMonGoi_CellClick);
            // 
            // columnMaSo
            // 
            this.columnMaSo.HeaderText = "Mã Số";
            this.columnMaSo.Name = "columnMaSo";
            this.columnMaSo.ReadOnly = true;
            this.columnMaSo.Visible = false;
            this.columnMaSo.Width = 59;
            // 
            // columnTenMonAn
            // 
            this.columnTenMonAn.HeaderText = "Tên Món Ăn";
            this.columnTenMonAn.Name = "columnTenMonAn";
            this.columnTenMonAn.ReadOnly = true;
            this.columnTenMonAn.Width = 91;
            // 
            // columnGia
            // 
            this.columnGia.HeaderText = "Giá";
            this.columnGia.Name = "columnGia";
            this.columnGia.ReadOnly = true;
            this.columnGia.Width = 48;
            // 
            // columnSoLuong
            // 
            this.columnSoLuong.HeaderText = "Số lượng";
            this.columnSoLuong.Name = "columnSoLuong";
            this.columnSoLuong.ReadOnly = true;
            this.columnSoLuong.Width = 74;
            // 
            // btnThemMonAn
            // 
            this.btnThemMonAn.BackColor = System.Drawing.Color.YellowGreen;
            this.btnThemMonAn.Location = new System.Drawing.Point(371, 279);
            this.btnThemMonAn.Margin = new System.Windows.Forms.Padding(2);
            this.btnThemMonAn.Name = "btnThemMonAn";
            this.btnThemMonAn.Size = new System.Drawing.Size(153, 45);
            this.btnThemMonAn.TabIndex = 2;
            this.btnThemMonAn.Text = "Thêm >>";
            this.btnThemMonAn.UseVisualStyleBackColor = false;
            this.btnThemMonAn.Click += new System.EventHandler(this.btnThemMonAn_Click);
            // 
            // btnXoaMonAn
            // 
            this.btnXoaMonAn.BackColor = System.Drawing.Color.Tomato;
            this.btnXoaMonAn.Location = new System.Drawing.Point(315, 327);
            this.btnXoaMonAn.Margin = new System.Windows.Forms.Padding(2);
            this.btnXoaMonAn.Name = "btnXoaMonAn";
            this.btnXoaMonAn.Size = new System.Drawing.Size(154, 43);
            this.btnXoaMonAn.TabIndex = 3;
            this.btnXoaMonAn.Text = "<< Bỏ";
            this.btnXoaMonAn.UseVisualStyleBackColor = false;
            this.btnXoaMonAn.Click += new System.EventHandler(this.btnXoaMonAn_Click);
            // 
            // btnGoiMon
            // 
            this.btnGoiMon.BackColor = System.Drawing.Color.YellowGreen;
            this.btnGoiMon.Location = new System.Drawing.Point(228, 495);
            this.btnGoiMon.Margin = new System.Windows.Forms.Padding(2);
            this.btnGoiMon.Name = "btnGoiMon";
            this.btnGoiMon.Size = new System.Drawing.Size(206, 67);
            this.btnGoiMon.TabIndex = 4;
            this.btnGoiMon.Text = "Gọi món";
            this.btnGoiMon.UseVisualStyleBackColor = false;
            this.btnGoiMon.Click += new System.EventHandler(this.btnGoiMon_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.BackColor = System.Drawing.Color.Tomato;
            this.btnHuy.Location = new System.Drawing.Point(438, 495);
            this.btnHuy.Margin = new System.Windows.Forms.Padding(2);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(183, 67);
            this.btnHuy.TabIndex = 5;
            this.btnHuy.Text = "Huỷ";
            this.btnHuy.UseVisualStyleBackColor = false;
            this.btnHuy.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtMaBanAn
            // 
            this.txtMaBanAn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtMaBanAn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBanAn.Location = new System.Drawing.Point(315, 12);
            this.txtMaBanAn.Name = "txtMaBanAn";
            this.txtMaBanAn.ReadOnly = true;
            this.txtMaBanAn.Size = new System.Drawing.Size(67, 26);
            this.txtMaBanAn.TabIndex = 7;
            this.txtMaBanAn.Text = "ma ban";
            // 
            // txtThoiGianVaoBan
            // 
            this.txtThoiGianVaoBan.Location = new System.Drawing.Point(388, 17);
            this.txtThoiGianVaoBan.Name = "txtThoiGianVaoBan";
            this.txtThoiGianVaoBan.ReadOnly = true;
            this.txtThoiGianVaoBan.Size = new System.Drawing.Size(136, 20);
            this.txtThoiGianVaoBan.TabIndex = 9;
            this.txtThoiGianVaoBan.Text = "thoi gian vao ban";
            this.txtThoiGianVaoBan.TextChanged += new System.EventHandler(this.txtThoiGianVaoBan_TextChanged);
            // 
            // txtSoLuongThem
            // 
            this.txtSoLuongThem.Font = new System.Drawing.Font("Microsoft Sans Serif", 23.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongThem.Location = new System.Drawing.Point(315, 279);
            this.txtSoLuongThem.Name = "txtSoLuongThem";
            this.txtSoLuongThem.Size = new System.Drawing.Size(50, 43);
            this.txtSoLuongThem.TabIndex = 10;
            this.txtSoLuongThem.Text = "1";
            // 
            // txtSoLuongGiam
            // 
            this.txtSoLuongGiam.Font = new System.Drawing.Font("Microsoft Sans Serif", 23.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongGiam.Location = new System.Drawing.Point(474, 327);
            this.txtSoLuongGiam.Name = "txtSoLuongGiam";
            this.txtSoLuongGiam.Size = new System.Drawing.Size(50, 43);
            this.txtSoLuongGiam.TabIndex = 11;
            this.txtSoLuongGiam.Text = "1";
            // 
            // txtTenMonAn
            // 
            this.txtTenMonAn.BackColor = System.Drawing.Color.Gold;
            this.txtTenMonAn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenMonAn.Location = new System.Drawing.Point(315, 178);
            this.txtTenMonAn.Name = "txtTenMonAn";
            this.txtTenMonAn.Size = new System.Drawing.Size(209, 31);
            this.txtTenMonAn.TabIndex = 12;
            this.txtTenMonAn.Text = "Tên món ăn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(315, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Tên món ăn:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(315, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Số lượng:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(472, 373);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Số lượng:";
            // 
            // FormGoiMon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 573);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTenMonAn);
            this.Controls.Add(this.txtSoLuongGiam);
            this.Controls.Add(this.txtSoLuongThem);
            this.Controls.Add(this.txtThoiGianVaoBan);
            this.Controls.Add(this.txtMaBanAn);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.btnGoiMon);
            this.Controls.Add(this.btnXoaMonAn);
            this.Controls.Add(this.btnThemMonAn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormGoiMon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormGoiMon";
            this.Load += new System.EventHandler(this.FormGoiMon_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataMonAn)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataMonGoi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataMonAn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnThemMonAn;
        private System.Windows.Forms.Button btnXoaMonAn;
        private System.Windows.Forms.Button btnGoiMon;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.DataGridView dataMonGoi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTimKiem;
        private System.Windows.Forms.TextBox txtMaBanAn;
        private System.Windows.Forms.TextBox txtThoiGianVaoBan;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnMaSo;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnTenMonAn;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnGia;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSoLuong;
        private System.Windows.Forms.TextBox txtSoLuongThem;
        private System.Windows.Forms.TextBox txtSoLuongGiam;
        private System.Windows.Forms.TextBox txtTenMonAn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}