﻿using Cua_Hang_An_Vat.Folder_Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.GUI
{
    public partial class FormGoiMon : Form
    {
        GoiMonController goiMonController;

        Main mainForm;

        public Main MainForm
        {
            get { return mainForm; }
            set { mainForm = value; }
        }

        private string maBanAn;

        public string MaBanAn
        {
            get { return maBanAn; }
            set { maBanAn = value; }
        }

        private string thoiGianVaoBan;

        public string ThoiGianVaoBan
        {
            get { return thoiGianVaoBan; }
            set { thoiGianVaoBan = value; }
        }


        public FormGoiMon(Main mainForm)
        {
            this.mainForm = mainForm;
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            mainForm.hienThiThongTinBanAn(maBanAn);
            this.Close();
        }

        private void khoiTao()
        {
            Program.khoiTaoControl(this);

        }
        private void FormGoiMon_Load(object sender, EventArgs e)
        {
            khoiTao();
            this.Text ="Gọi món: " + MaBanAn;
            txtMaBanAn.Text = MaBanAn;
            txtThoiGianVaoBan.Text = ThoiGianVaoBan;

            goiMonController = new GoiMonController(txtTimKiem, txtMaBanAn, txtThoiGianVaoBan, txtTenMonAn,txtSoLuongThem,txtSoLuongGiam, dataMonAn, dataMonGoi, btnThemMonAn, btnXoaMonAn, btnGoiMon);

           

        }

        private void txtThoiGianVaoBan_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataMonAn_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            goiMonController.hienThiThongTinMonAnDaChon(e.RowIndex);
        }

        private void btnThemMonAn_Click(object sender, EventArgs e)
        {
            goiMonController.themMonAn();
        }

        private void btnXoaMonAn_Click(object sender, EventArgs e)
        {
            goiMonController.xoaMonAn();
        }

        private void dataMonGoi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            goiMonController.hienThiThongTinMonAnDaGoi(e.RowIndex);
        }

        private void btnGoiMon_Click(object sender, EventArgs e)
        {
            goiMonController.goiMon();
            mainForm.loadBanAn();
            mainForm.hienThiThongTinBanAn(maBanAn);
            this.Close();
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            goiMonController.timKiemMonAn();
        }
    }
}
