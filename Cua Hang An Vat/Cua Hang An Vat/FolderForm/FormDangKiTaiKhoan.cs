﻿using Cua_Hang_An_Vat.Folder_Controller;
using Cua_Hang_An_Vat.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.FolderForm
{
    public partial class FormDangKiTaiKhoan : Form
    {
        FormQuanLyTaiKhoan formQuanLyTaiKhoan;

        public FormQuanLyTaiKhoan FormQuanLyTaiKhoan
        {
            get { return formQuanLyTaiKhoan; }
            set { formQuanLyTaiKhoan = value; }
        }

        TaiKhoanController taiKhoanController;
        public FormDangKiTaiKhoan()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormDangKiTaiKhoan_Load(object sender, EventArgs e)

        {
            khoiTao();
            taiKhoanController = new TaiKhoanController(txtTenTaiKhoan, txtMatKhau, txtTenNhanVien, dateNgaySinh, txtSoDienThoai, txtDiaChi);
        }
        private void khoiTao()
        {
            Program.khoiTaoControl(this);

        }
        private void button1_Click(object sender, EventArgs e)
        {
            taiKhoanController.themTaiKhoan();
            formQuanLyTaiKhoan.refreshDataTaiKhoan();
            this.Close();
        }
    }
}
