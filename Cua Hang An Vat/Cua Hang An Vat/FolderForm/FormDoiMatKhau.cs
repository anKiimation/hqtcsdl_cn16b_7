﻿using Cua_Hang_An_Vat.Folder_Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.FolderForm
{
    public partial class FormDoiMatKhau : Form
    {
        TaiKhoanController taiKhoanController;
        DangNhapController dangNhapController;
        string tenTaiKhoan;

        public string TenTaiKhoan
        {
            get { return tenTaiKhoan; }
            set { tenTaiKhoan = value; }
        }

        public FormDoiMatKhau()
        {
            InitializeComponent();
        }

        private void khoiTao()
        {
            Program.khoiTaoControl(this);
        }
        private void FormDoiMatKhau_Load(object sender, EventArgs e)
        {
            khoiTao();
            this.Text = "Đổi mật khẩu: " + tenTaiKhoan;
            taiKhoanController = new TaiKhoanController();
            dangNhapController = new DangNhapController();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtMatKhauCu.Text.Equals("") || txtMatKhauMoi.Text.Equals("") || txtNhapLaiMatKhauMoi.Text.Equals(""))
            {
                MessageBox.Show("Đùa bố mày?!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string matKhauCu = taiKhoanController.taoMatKhau(txtMatKhauCu.Text);
                string matKhauMoi = taiKhoanController.taoMatKhau(txtMatKhauMoi.Text);
                string matKhauMoi2 = taiKhoanController.taoMatKhau(txtNhapLaiMatKhauMoi.Text);

                if (dangNhapController.dangNhap(tenTaiKhoan, txtMatKhauCu.Text))
                {
                    if (matKhauMoi.Equals(matKhauMoi2))
                    {
                        taiKhoanController.doiMatKhau(tenTaiKhoan, matKhauMoi);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Mật khẩu mới không trùng khớp!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        clearTextBox();
                    }
                }
                else
                {
                    MessageBox.Show("Mật khẩu cũ không đúng!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearTextBox();
                }
            }
          
            
        }
        private void clearTextBox()
        {
            txtMatKhauCu.Text = "";
            txtMatKhauMoi.Text = "";
            txtNhapLaiMatKhauMoi.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
