﻿using Cua_Hang_An_Vat.Folder_Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.FolderForm
{
    public partial class FormQuanLyHoaDon : Form
    {
        HoaDonController hoaDonController;
        public FormQuanLyHoaDon()
        {
            InitializeComponent();
        }

        private void khoiTao()
        {
            Program.khoiTaoControl(this);
        }
        private void FormQuanLyHoaDon_Load(object sender, EventArgs e)
        {
            khoiTao();
            hoaDonController = new HoaDonController(dataDanhSachHoaDon, txtTimKiem, txtBan, txtNhanVien, txtThoiGianVaoBan, txtTongSoMonAn, txtTongTien, listMonAn);
        }

        private void dataDanhSachHoaDon_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            hoaDonController.hienThiThongTinHoaDon(e.RowIndex);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtTongTien_TextChanged(object sender, EventArgs e)
        {
            Program.tienTe(txtTongTien);
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            hoaDonController.timKiemHoaDon();
        }
    }
}
