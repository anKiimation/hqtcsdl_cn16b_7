﻿namespace Cua_Hang_An_Vat.GUI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnChuyenBan = new System.Windows.Forms.Button();
            this.txtThoiGianVaoBanRutGon = new System.Windows.Forms.TextBox();
            this.comboDanhSachBanTrong = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTongTien = new System.Windows.Forms.TextBox();
            this.txtTongSoMonAn = new System.Windows.Forms.TextBox();
            this.txtThoiGianVaoBan = new System.Windows.Forms.TextBox();
            this.txtMaSo = new System.Windows.Forms.TextBox();
            this.btnThanhToan = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataDanhSachMonAn = new System.Windows.Forms.DataGridView();
            this.btnGoiMon = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.helloTab = new System.Windows.Forms.ToolStripMenuItem();
            this.đổiMậtKhẩuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.đăngXuấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quanLyTab = new System.Windows.Forms.ToolStripMenuItem();
            this.thêmBànĂnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýHoáĐơnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tàiKhoảnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xemDoanhThuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.congCuTab = new System.Windows.Forms.ToolStripMenuItem();
            this.mởMáyTínhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelBanAn = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataDanhSachMonAn)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnChuyenBan);
            this.groupBox2.Controls.Add(this.txtThoiGianVaoBanRutGon);
            this.groupBox2.Controls.Add(this.comboDanhSachBanTrong);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtTongTien);
            this.groupBox2.Controls.Add(this.txtTongSoMonAn);
            this.groupBox2.Controls.Add(this.txtThoiGianVaoBan);
            this.groupBox2.Controls.Add(this.txtMaSo);
            this.groupBox2.Controls.Add(this.btnThanhToan);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(365, 418);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(326, 296);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin bàn";
            // 
            // btnChuyenBan
            // 
            this.btnChuyenBan.Location = new System.Drawing.Point(239, 41);
            this.btnChuyenBan.Name = "btnChuyenBan";
            this.btnChuyenBan.Size = new System.Drawing.Size(81, 23);
            this.btnChuyenBan.TabIndex = 2;
            this.btnChuyenBan.Text = "Chuyển bàn";
            this.btnChuyenBan.UseVisualStyleBackColor = true;
            this.btnChuyenBan.Click += new System.EventHandler(this.btnChuyenBan_Click);
            // 
            // txtThoiGianVaoBanRutGon
            // 
            this.txtThoiGianVaoBanRutGon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiGianVaoBanRutGon.Location = new System.Drawing.Point(68, 71);
            this.txtThoiGianVaoBanRutGon.Name = "txtThoiGianVaoBanRutGon";
            this.txtThoiGianVaoBanRutGon.ReadOnly = true;
            this.txtThoiGianVaoBanRutGon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThoiGianVaoBanRutGon.Size = new System.Drawing.Size(252, 29);
            this.txtThoiGianVaoBanRutGon.TabIndex = 11;
            // 
            // comboDanhSachBanTrong
            // 
            this.comboDanhSachBanTrong.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDanhSachBanTrong.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboDanhSachBanTrong.FormattingEnabled = true;
            this.comboDanhSachBanTrong.Location = new System.Drawing.Point(239, 16);
            this.comboDanhSachBanTrong.Name = "comboDanhSachBanTrong";
            this.comboDanhSachBanTrong.Size = new System.Drawing.Size(81, 24);
            this.comboDanhSachBanTrong.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(307, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "đ";
            // 
            // txtTongTien
            // 
            this.txtTongTien.BackColor = System.Drawing.Color.Beige;
            this.txtTongTien.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTien.Location = new System.Drawing.Point(12, 171);
            this.txtTongTien.Name = "txtTongTien";
            this.txtTongTien.ReadOnly = true;
            this.txtTongTien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTien.Size = new System.Drawing.Size(298, 47);
            this.txtTongTien.TabIndex = 9;
            this.txtTongTien.TextChanged += new System.EventHandler(this.txtTongTien_TextChanged);
            this.txtTongTien.Leave += new System.EventHandler(this.txtTongTien_Leave);
            // 
            // txtTongSoMonAn
            // 
            this.txtTongSoMonAn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoMonAn.Location = new System.Drawing.Point(123, 106);
            this.txtTongSoMonAn.Name = "txtTongSoMonAn";
            this.txtTongSoMonAn.ReadOnly = true;
            this.txtTongSoMonAn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoMonAn.Size = new System.Drawing.Size(197, 29);
            this.txtTongSoMonAn.TabIndex = 7;
            // 
            // txtThoiGianVaoBan
            // 
            this.txtThoiGianVaoBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThoiGianVaoBan.Location = new System.Drawing.Point(68, 71);
            this.txtThoiGianVaoBan.Name = "txtThoiGianVaoBan";
            this.txtThoiGianVaoBan.ReadOnly = true;
            this.txtThoiGianVaoBan.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThoiGianVaoBan.Size = new System.Drawing.Size(221, 29);
            this.txtThoiGianVaoBan.TabIndex = 6;
            this.txtThoiGianVaoBan.Text = "10:20";
            this.txtThoiGianVaoBan.Visible = false;
            // 
            // txtMaSo
            // 
            this.txtMaSo.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSo.Location = new System.Drawing.Point(68, 16);
            this.txtMaSo.Name = "txtMaSo";
            this.txtMaSo.ReadOnly = true;
            this.txtMaSo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtMaSo.Size = new System.Drawing.Size(165, 49);
            this.txtMaSo.TabIndex = 5;
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.BackColor = System.Drawing.SystemColors.Control;
            this.btnThanhToan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnThanhToan.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThanhToan.Location = new System.Drawing.Point(12, 226);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(298, 63);
            this.btnThanhToan.TabIndex = 0;
            this.btnThanhToan.Text = "Thanh toán";
            this.btnThanhToan.UseVisualStyleBackColor = false;
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tổng tiền:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tổng số món ăn:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Giờ vào:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bàn số:";
            // 
            // dataDanhSachMonAn
            // 
            this.dataDanhSachMonAn.AllowUserToAddRows = false;
            this.dataDanhSachMonAn.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.dataDanhSachMonAn.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataDanhSachMonAn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataDanhSachMonAn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataDanhSachMonAn.EnableHeadersVisualStyles = false;
            this.dataDanhSachMonAn.GridColor = System.Drawing.Color.Black;
            this.dataDanhSachMonAn.Location = new System.Drawing.Point(6, 20);
            this.dataDanhSachMonAn.Name = "dataDanhSachMonAn";
            this.dataDanhSachMonAn.ReadOnly = true;
            this.dataDanhSachMonAn.RowHeadersVisible = false;
            this.dataDanhSachMonAn.Size = new System.Drawing.Size(331, 221);
            this.dataDanhSachMonAn.TabIndex = 0;
            this.dataDanhSachMonAn.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataDanhSachMonAn_CellContentClick);
            // 
            // btnGoiMon
            // 
            this.btnGoiMon.BackColor = System.Drawing.Color.SkyBlue;
            this.btnGoiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoiMon.Location = new System.Drawing.Point(184, 248);
            this.btnGoiMon.Name = "btnGoiMon";
            this.btnGoiMon.Size = new System.Drawing.Size(153, 41);
            this.btnGoiMon.TabIndex = 1;
            this.btnGoiMon.Text = "Gọi món";
            this.btnGoiMon.UseVisualStyleBackColor = false;
            this.btnGoiMon.Click += new System.EventHandler(this.btnGoiMon_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnGoiMon);
            this.groupBox3.Controls.Add(this.dataDanhSachMonAn);
            this.groupBox3.Location = new System.Drawing.Point(12, 418);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(347, 296);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Danh sách món ăn";
            // 
            // helloTab
            // 
            this.helloTab.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.đổiMậtKhẩuToolStripMenuItem,
            this.đăngXuấtToolStripMenuItem});
            this.helloTab.Name = "helloTab";
            this.helloTab.Size = new System.Drawing.Size(68, 20);
            this.helloTab.Text = "Xin chào:";
            // 
            // đổiMậtKhẩuToolStripMenuItem
            // 
            this.đổiMậtKhẩuToolStripMenuItem.Name = "đổiMậtKhẩuToolStripMenuItem";
            this.đổiMậtKhẩuToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.đổiMậtKhẩuToolStripMenuItem.Text = "Đổi mật khẩu";
            this.đổiMậtKhẩuToolStripMenuItem.Click += new System.EventHandler(this.đổiMậtKhẩuToolStripMenuItem_Click);
            // 
            // đăngXuấtToolStripMenuItem
            // 
            this.đăngXuấtToolStripMenuItem.Name = "đăngXuấtToolStripMenuItem";
            this.đăngXuấtToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.đăngXuấtToolStripMenuItem.Text = "Đăng xuất";
            this.đăngXuấtToolStripMenuItem.Click += new System.EventHandler(this.đăngXuấtToolStripMenuItem_Click);
            // 
            // quanLyTab
            // 
            this.quanLyTab.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thêmBànĂnToolStripMenuItem,
            this.menuToolStripMenuItem,
            this.quảnLýHoáĐơnToolStripMenuItem,
            this.tàiKhoảnToolStripMenuItem,
            this.xemDoanhThuToolStripMenuItem});
            this.quanLyTab.Enabled = false;
            this.quanLyTab.Name = "quanLyTab";
            this.quanLyTab.Size = new System.Drawing.Size(60, 20);
            this.quanLyTab.Text = "Quản lý";
            this.quanLyTab.Click += new System.EventHandler(this.quanLyTab_Click);
            // 
            // thêmBànĂnToolStripMenuItem
            // 
            this.thêmBànĂnToolStripMenuItem.Name = "thêmBànĂnToolStripMenuItem";
            this.thêmBànĂnToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.thêmBànĂnToolStripMenuItem.Text = "Bàn ăn";
            this.thêmBànĂnToolStripMenuItem.Click += new System.EventHandler(this.thêmBànĂnToolStripMenuItem_Click);
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.menuToolStripMenuItem.Text = "Thực đơn";
            this.menuToolStripMenuItem.Click += new System.EventHandler(this.menuToolStripMenuItem_Click);
            // 
            // quảnLýHoáĐơnToolStripMenuItem
            // 
            this.quảnLýHoáĐơnToolStripMenuItem.Name = "quảnLýHoáĐơnToolStripMenuItem";
            this.quảnLýHoáĐơnToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.quảnLýHoáĐơnToolStripMenuItem.Text = "Hoá đơn";
            this.quảnLýHoáĐơnToolStripMenuItem.Click += new System.EventHandler(this.quảnLýHoáĐơnToolStripMenuItem_Click);
            // 
            // tàiKhoảnToolStripMenuItem
            // 
            this.tàiKhoảnToolStripMenuItem.Name = "tàiKhoảnToolStripMenuItem";
            this.tàiKhoảnToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.tàiKhoảnToolStripMenuItem.Text = "Tài khoản";
            this.tàiKhoảnToolStripMenuItem.Click += new System.EventHandler(this.tàiKhoảnToolStripMenuItem_Click);
            // 
            // xemDoanhThuToolStripMenuItem
            // 
            this.xemDoanhThuToolStripMenuItem.Name = "xemDoanhThuToolStripMenuItem";
            this.xemDoanhThuToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.xemDoanhThuToolStripMenuItem.Text = "Tình trạng cửa hàng";
            this.xemDoanhThuToolStripMenuItem.Click += new System.EventHandler(this.xemDoanhThuToolStripMenuItem_Click);
            // 
            // congCuTab
            // 
            this.congCuTab.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mởMáyTínhToolStripMenuItem});
            this.congCuTab.Name = "congCuTab";
            this.congCuTab.Size = new System.Drawing.Size(64, 20);
            this.congCuTab.Text = "Công cụ";
            this.congCuTab.Click += new System.EventHandler(this.cÔngCụToolStripMenuItem_Click);
            // 
            // mởMáyTínhToolStripMenuItem
            // 
            this.mởMáyTínhToolStripMenuItem.Name = "mởMáyTínhToolStripMenuItem";
            this.mởMáyTínhToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.mởMáyTínhToolStripMenuItem.Text = "Mở máy tính";
            this.mởMáyTínhToolStripMenuItem.Click += new System.EventHandler(this.mởMáyTínhToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helloTab,
            this.quanLyTab,
            this.congCuTab});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(703, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panelBanAn);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(679, 385);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh sách bàn ăn";
            // 
            // panelBanAn
            // 
            this.panelBanAn.AutoScroll = true;
            this.panelBanAn.Location = new System.Drawing.Point(6, 20);
            this.panelBanAn.Name = "panelBanAn";
            this.panelBanAn.Size = new System.Drawing.Size(667, 354);
            this.panelBanAn.TabIndex = 0;
            this.panelBanAn.Paint += new System.Windows.Forms.PaintEventHandler(this.panelBanAn_Paint);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(703, 721);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QUẢN LÝ CỬA HÀNG ĂN VẶT";
            this.Load += new System.EventHandler(this.Main_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataDanhSachMonAn)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnThanhToan;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTongTien;
        private System.Windows.Forms.TextBox txtTongSoMonAn;
        private System.Windows.Forms.TextBox txtThoiGianVaoBan;
        private System.Windows.Forms.TextBox txtMaSo;
        private System.Windows.Forms.DataGridView dataDanhSachMonAn;
        private System.Windows.Forms.Button btnGoiMon;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem helloTab;
        private System.Windows.Forms.ToolStripMenuItem quanLyTab;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem congCuTab;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel panelBanAn;
        private System.Windows.Forms.ToolStripMenuItem tàiKhoảnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem đổiMậtKhẩuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem đăngXuấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mởMáyTínhToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtThoiGianVaoBanRutGon;
        private System.Windows.Forms.ToolStripMenuItem quảnLýHoáĐơnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xemDoanhThuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thêmBànĂnToolStripMenuItem;
        private System.Windows.Forms.Button btnChuyenBan;
        private System.Windows.Forms.ComboBox comboDanhSachBanTrong;
    }
}