﻿namespace Cua_Hang_An_Vat.FolderForm
{
    partial class FormTinhTrangCuaHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboNgay = new System.Windows.Forms.ComboBox();
            this.comboThang = new System.Windows.Forms.ComboBox();
            this.comboNam = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTongDoanhThu = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataBanAnNgoiNhieuNhat = new System.Windows.Forms.DataGridView();
            this.dataBanAnCoDoanhThuCaoNhat = new System.Windows.Forms.DataGridView();
            this.dataMonAnGoiNhieuNhat = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataBanAnNgoiNhieuNhat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBanAnCoDoanhThuCaoNhat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataMonAnGoiNhieuNhat)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtTongDoanhThu);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dataBanAnNgoiNhieuNhat);
            this.groupBox1.Controls.Add(this.dataBanAnCoDoanhThuCaoNhat);
            this.groupBox1.Controls.Add(this.dataMonAnGoiNhieuNhat);
            this.groupBox1.Controls.Add(this.comboNam);
            this.groupBox1.Controls.Add(this.comboThang);
            this.groupBox1.Controls.Add(this.comboNgay);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(736, 572);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Doanh thu";
            // 
            // comboNgay
            // 
            this.comboNgay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboNgay.FormattingEnabled = true;
            this.comboNgay.Location = new System.Drawing.Point(160, 19);
            this.comboNgay.Name = "comboNgay";
            this.comboNgay.Size = new System.Drawing.Size(121, 21);
            this.comboNgay.TabIndex = 2;
            this.comboNgay.SelectedIndexChanged += new System.EventHandler(this.comboNgay_SelectedIndexChanged);
            // 
            // comboThang
            // 
            this.comboThang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboThang.FormattingEnabled = true;
            this.comboThang.Location = new System.Drawing.Point(287, 19);
            this.comboThang.Name = "comboThang";
            this.comboThang.Size = new System.Drawing.Size(121, 21);
            this.comboThang.TabIndex = 3;
            this.comboThang.SelectedIndexChanged += new System.EventHandler(this.comboThang_SelectedIndexChanged);
            // 
            // comboNam
            // 
            this.comboNam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboNam.FormattingEnabled = true;
            this.comboNam.Location = new System.Drawing.Point(414, 19);
            this.comboNam.Name = "comboNam";
            this.comboNam.Size = new System.Drawing.Size(121, 21);
            this.comboNam.TabIndex = 4;
            this.comboNam.SelectedIndexChanged += new System.EventHandler(this.comboNam_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(713, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 18);
            this.label7.TabIndex = 25;
            this.label7.Text = "đ";
            // 
            // txtTongDoanhThu
            // 
            this.txtTongDoanhThu.BackColor = System.Drawing.Color.Gold;
            this.txtTongDoanhThu.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongDoanhThu.ForeColor = System.Drawing.Color.DarkGreen;
            this.txtTongDoanhThu.Location = new System.Drawing.Point(382, 159);
            this.txtTongDoanhThu.Name = "txtTongDoanhThu";
            this.txtTongDoanhThu.ReadOnly = true;
            this.txtTongDoanhThu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongDoanhThu.Size = new System.Drawing.Size(325, 40);
            this.txtTongDoanhThu.TabIndex = 24;
            this.txtTongDoanhThu.Text = "10000";
            this.txtTongDoanhThu.TextChanged += new System.EventHandler(this.txtTongDoanhThu_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(192, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(184, 25);
            this.label4.TabIndex = 23;
            this.label4.Text = "Tổng doanh thu:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(476, 223);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Số lần ngồi vào bàn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(247, 223);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Doanh thu theo bàn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 223);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Số lần gọi món";
            // 
            // dataBanAnNgoiNhieuNhat
            // 
            this.dataBanAnNgoiNhieuNhat.AllowUserToAddRows = false;
            this.dataBanAnNgoiNhieuNhat.AllowUserToDeleteRows = false;
            this.dataBanAnNgoiNhieuNhat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataBanAnNgoiNhieuNhat.Location = new System.Drawing.Point(479, 239);
            this.dataBanAnNgoiNhieuNhat.Name = "dataBanAnNgoiNhieuNhat";
            this.dataBanAnNgoiNhieuNhat.ReadOnly = true;
            this.dataBanAnNgoiNhieuNhat.Size = new System.Drawing.Size(250, 323);
            this.dataBanAnNgoiNhieuNhat.TabIndex = 16;
            // 
            // dataBanAnCoDoanhThuCaoNhat
            // 
            this.dataBanAnCoDoanhThuCaoNhat.AllowUserToAddRows = false;
            this.dataBanAnCoDoanhThuCaoNhat.AllowUserToDeleteRows = false;
            this.dataBanAnCoDoanhThuCaoNhat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataBanAnCoDoanhThuCaoNhat.Location = new System.Drawing.Point(250, 239);
            this.dataBanAnCoDoanhThuCaoNhat.Name = "dataBanAnCoDoanhThuCaoNhat";
            this.dataBanAnCoDoanhThuCaoNhat.ReadOnly = true;
            this.dataBanAnCoDoanhThuCaoNhat.Size = new System.Drawing.Size(223, 323);
            this.dataBanAnCoDoanhThuCaoNhat.TabIndex = 15;
            // 
            // dataMonAnGoiNhieuNhat
            // 
            this.dataMonAnGoiNhieuNhat.AllowUserToAddRows = false;
            this.dataMonAnGoiNhieuNhat.AllowUserToDeleteRows = false;
            this.dataMonAnGoiNhieuNhat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataMonAnGoiNhieuNhat.Location = new System.Drawing.Point(6, 239);
            this.dataMonAnGoiNhieuNhat.Name = "dataMonAnGoiNhieuNhat";
            this.dataMonAnGoiNhieuNhat.ReadOnly = true;
            this.dataMonAnGoiNhieuNhat.Size = new System.Drawing.Size(238, 323);
            this.dataMonAnGoiNhieuNhat.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(162, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(373, 33);
            this.label5.TabIndex = 26;
            this.label5.Text = "TÌNH TRẠNG CỬA HÀNG";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Tomato;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(13, 592);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(376, 68);
            this.button1.TabIndex = 1;
            this.button1.Text = "Đóng";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.SkyBlue;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(395, 592);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(354, 68);
            this.button2.TabIndex = 2;
            this.button2.Text = "In";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // FormTinhTrangCuaHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 672);
            this.ControlBox = false;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormTinhTrangCuaHang";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tình trạng cửa hàng";
            this.Load += new System.EventHandler(this.FormTinhTrangCuaHang_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataBanAnNgoiNhieuNhat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBanAnCoDoanhThuCaoNhat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataMonAnGoiNhieuNhat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboNam;
        private System.Windows.Forms.ComboBox comboThang;
        private System.Windows.Forms.ComboBox comboNgay;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTongDoanhThu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataBanAnNgoiNhieuNhat;
        private System.Windows.Forms.DataGridView dataBanAnCoDoanhThuCaoNhat;
        private System.Windows.Forms.DataGridView dataMonAnGoiNhieuNhat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}