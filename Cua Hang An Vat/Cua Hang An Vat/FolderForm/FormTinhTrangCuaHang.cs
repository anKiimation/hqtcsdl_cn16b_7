﻿using Cua_Hang_An_Vat.Folder_Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.FolderForm
{
    public partial class FormTinhTrangCuaHang : Form
    {
        TinhTrangCuaHangController tinhTrangCuaHangController;
        public FormTinhTrangCuaHang()
        {
            InitializeComponent();
        }

        private void khoiTao()
        {
            Program.khoiTaoControl(this);
        }
        private void FormTinhTrangCuaHang_Load(object sender, EventArgs e)
        {
            khoiTao();
            tinhTrangCuaHangController = new TinhTrangCuaHangController(comboNgay, comboThang, comboNam, txtTongDoanhThu, dataMonAnGoiNhieuNhat, dataBanAnCoDoanhThuCaoNhat, dataBanAnNgoiNhieuNhat);
            comboNam.Text = "2019";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clearNgayThangNam();
        }

        private void clearNgayThangNam()
        {
            comboNgay.Items.Clear();
            comboThang.Items.Clear();
            comboNam.Items.Clear();

        }

        private void comboNgay_SelectedIndexChanged(object sender, EventArgs e)
        {
            tinhTrangCuaHangController.chonNgay();
        }

        private void comboThang_SelectedIndexChanged(object sender, EventArgs e)
        {
            tinhTrangCuaHangController.chonThang();
        }

        private void comboNam_SelectedIndexChanged(object sender, EventArgs e)
        {
            tinhTrangCuaHangController.chonNam();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtTongDoanhThu_TextChanged(object sender, EventArgs e)
        {
            Program.tienTe(txtTongDoanhThu);
        }


    }
}
