﻿using Cua_Hang_An_Vat.DTO;
using Cua_Hang_An_Vat.Folder_Database;
using Cua_Hang_An_Vat.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.FolderForm
{
    public partial class FormThanhToan : Form
    {
        Main mainForm;

        public Main MainForm
        {
            get { return mainForm; }
            set { mainForm = value; }
        }
        string tenNhanVien;

        public string TenNhanVien
        {
            get { return tenNhanVien; }
            set { tenNhanVien = value; }
        }
        string maBanAn;

        public string MaBanAn
        {
            get { return maBanAn; }
            set { maBanAn = value; }
        }
        string thoiGianVao;

        public string ThoiGianVao
        {
            get { return thoiGianVao; }
            set { thoiGianVao = value; }
        }
        int tongSoMonAn;

        public int TongSoMonAn
        {
            get { return tongSoMonAn; }
            set { tongSoMonAn = value; }
        }
        long tongTien;

        public long TongTien
        {
            get { return tongTien; }
            set { tongTien = value; }
        }





        public FormThanhToan(string tenNhanVien, string maBanAn, string thoiGianVao, int tongSoMonAn, long tongTien, Main mainForm)
        {
            this.tenNhanVien = tenNhanVien;
            this.maBanAn = maBanAn;
            this.thoiGianVao = thoiGianVao;
            this.tongSoMonAn = tongSoMonAn;
            this.tongTien = tongTien;
            this.mainForm = mainForm;
            InitializeComponent();
        }


        public FormThanhToan()
        {
            InitializeComponent();
        }

        private void khoiTao()
        {
            Program.khoiTaoControl(this);

        }
        private void FormThanhToan_Load(object sender, EventArgs e)
        {
            khoiTao();
            txtNhanVien.Text = tenNhanVien;
            txtBan.Text = maBanAn;
            txtTongSoMonAn.Text = tongSoMonAn +"";
            txtTongTien.Text = tongTien+"";
            txtThoiGianVaoBan.Text = thoiGianVao;
            this.Text = maBanAn;
            List<MonAn.MonAnDaGoi> lst = (new SQLGoiMon()).loadListGoiMon(maBanAn, Convert.ToDateTime(thoiGianVao));
            foreach (MonAn.MonAnDaGoi i in lst)
            {
                string[] row = {i.TenMonAn, i.GiaTien.ToString(), i.SoLuong.ToString()};
                ListViewItem a = new ListViewItem(row);
                listMonAn.Items.Add(a);
            }
            listMonAn.Columns[0].Width = -2;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            mainForm.Show();
        }

        private void txtTongTien_TextChanged(object sender, EventArgs e)
        {
            Program.tienTe(txtTongTien);
        }
    }
}
