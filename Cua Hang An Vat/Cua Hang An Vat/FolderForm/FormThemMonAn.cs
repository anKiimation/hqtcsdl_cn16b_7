﻿using Cua_Hang_An_Vat.Folder_Controller;
using Cua_Hang_An_Vat.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.FolderForm
{
    public partial class FormThemMonAn : Form
    {
        FormMenu formMenu;

public FormMenu FormMenu
{
  get { return formMenu; }
  set { formMenu = value; }
}

     

        MonAnController monAnController;

        public FormThemMonAn()
        {
           
            InitializeComponent();
        }

        private void khoiTao()
        {
            Program.khoiTaoControl(this);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            monAnController.themMonAn();
            formMenu.refresh();
            this.Close();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormThemMonAn_Load(object sender, EventArgs e)
        {
            monAnController = new MonAnController(txtMaSo, txtTenMonAn, txtGiaTien, comboKhaDung);
            khoiTao();
        }

        private void txtGiaTien_TextChanged(object sender, EventArgs e)
        {
            Program.tienTe(txtGiaTien);
        }
    }
}
