﻿using Cua_Hang_An_Vat.BUS;
using Cua_Hang_An_Vat.DTO;
using Cua_Hang_An_Vat.Folder_Class;
using Cua_Hang_An_Vat.Folder_Controller;
using Cua_Hang_An_Vat.Folder_Database;
using Cua_Hang_An_Vat.FolderForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Cua_Hang_An_Vat.GUI
{
    public partial class Main : Form
    {
        TaiKhoan taiKhoan;

        internal TaiKhoan TaiKhoan
        {
            get { return taiKhoan; }
            set { taiKhoan = value; }
        }

          BanAnController banAnController ;
          GoiMonController goiMonController;

        public Main()
        {
            InitializeComponent();
        }

     


        private void cÔngCụToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private List<MonAn.MonAnDaGoi> lstMonAnDaGoi(string maSo, DateTime thoiGianVaoBan)
        {

            List<MonAn.MonAnDaGoi> lstTemp = (new SQLGoiMon()).loadListGoiMon(maSo, thoiGianVaoBan);
            return lstTemp;
        }

        public void thanhToan()
        {
            string maSo = txtMaSo.Text;
            string thoiGianVaoBan = txtThoiGianVaoBan.Text;
            int tongSoMonAn = Convert.ToInt32(txtTongSoMonAn.Text);
            long tongTien = Convert.ToInt64(txtTongTien.Text.Replace(",", ""));


            List<MonAn.MonAnDaGoi> lst = new List<MonAn.MonAnDaGoi>();
            lst = this.lstMonAnDaGoi(maSo, Convert.ToDateTime(thoiGianVaoBan));

            string tenNhanVien = taiKhoan.TenNhanVien;

            ( new FormThanhToan(tenNhanVien, maSo, thoiGianVaoBan, tongSoMonAn, tongTien,this)).Show();
           
            banAnController.traBan(maSo, Convert.ToDateTime(thoiGianVaoBan), taiKhoan.TenTaiKhoan);
        }
        public void traBan()
        {
            string maSo = txtMaSo.Text;
            string thoiGianVaoBan = txtThoiGianVaoBan.Text;
           
            banAnController.traBan(maSo, Convert.ToDateTime(thoiGianVaoBan), taiKhoan.TenTaiKhoan);
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64( txtTongTien.Text.Replace(",", "")) == 0) {

                traBan();
            }
            else
            {
                thanhToan();
                this.Hide();
            }
            
        }

        private void panelBanAn_Paint(object sender, PaintEventArgs e)
        {

        }

        public void kiemTraDangNhap()
        {
            if (taiKhoan.IsAdmin)
            {
                quanLyTab.Enabled = true;
            }
        }

        public void hienThiThongTinBanAn(string maSo)
        {
            banAnController.hienThongTinBan(maSo);

        }

        private void khoiTao() {
            Program.khoiTaoControl(this);
        
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //gan Control
            //banAnController.Panel = panelBanAn;
            //banAnController.TxtMaSo = txtBanSo;
            //banAnController.BtnGoiMon = btnGoiMon;
            //banAnController.BtnThanhToan = btnThanhToan;
            //banAnController.TxtThoiGianVaoBan = txtThoiGianVaoBan;
            //banAnController.TxtTongSoMonAn = txtTongSoMonAn;
            //banAnController.TxtKhuyenMai = txtKhuyenMai;
            //banAnController.TxtTongTien = txtTongTien;

           banAnController = new BanAnController(txtMaSo, txtThoiGianVaoBan,txtThoiGianVaoBanRutGon,  txtTongSoMonAn,  txtTongTien,  panelBanAn,  btnGoiMon,  btnThanhToan, btnChuyenBan,comboDanhSachBanTrong, dataDanhSachMonAn);

            //
            banAnController.loadBanAn();
        

            kiemTraDangNhap();
            helloTab.Text += " " + taiKhoan.TenNhanVien;
            khoiTao();

            
        }

        public void loadBanAn() {
            banAnController.loadBanAn();
           
            banAnController.toMauBan(txtMaSo.Text);
            
        }

        private void btnGoiMon_Click(object sender, EventArgs e)
        {
            string maSo = txtMaSo.Text;
            if (!maSo.Equals(""))
            {
                
                    banAnController.ngoiVaoBan(maSo);

           
                FormGoiMon formGoiMon = new FormGoiMon(this);
                formGoiMon.MaBanAn = maSo;
                formGoiMon.ThoiGianVaoBan = txtThoiGianVaoBan.Text;
                formGoiMon.Show();
            }
            else
            {
                MessageBox.Show("Chưa chọn bàn!", "Óc!");
            }

            
            

        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new FormMenu()).Show();
        }

        private void tàiKhoảnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new FormQuanLyTaiKhoan()).Show();
        }

        private void mởMáyTínhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void đăngXuấtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();

            
            //(new FormDangNhap()).Show();
            //this.Close();
        }

        private void txtKhuyenMai_TextChanged(object sender, EventArgs e)
        {

            //long tongTien = Convert.ToInt64(txtTongTien.Text);
            //try
            //{
               
            //    long khuyenMai = Convert.ToInt64(txtKhuyenMai.Text);
            //    tongTien -= khuyenMai;
              
            //}
            //catch { }

           
            
        }

        private void dataDanhSachMonAn_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void đổiMậtKhẩuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDoiMatKhau formDoiMatKhau = new FormDoiMatKhau();
            formDoiMatKhau.TenTaiKhoan = this.taiKhoan.TenTaiKhoan;
            formDoiMatKhau.Show();
        }

        private void quảnLýHoáĐơnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new FormQuanLyHoaDon()).Show();
        }

        private void thêmBànĂnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormBanAn formBanAn = new FormBanAn();
            formBanAn.MainForm = this;
            formBanAn.Show();
        }

        private void xemDoanhThuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new FormTinhTrangCuaHang()).Show();
        }

        private void btnChuyenBan_Click(object sender, EventArgs e)
        {
            banAnController.chuyenBan();
        }

        private void txtTongTien_TextChanged(object sender, EventArgs e)
        {
            Program.tienTe(txtTongTien);
        }

        private void txtTongTien_Leave(object sender, EventArgs e)
        {
           
        }

        private void quanLyTab_Click(object sender, EventArgs e)
        {

        }

       
    }
}
