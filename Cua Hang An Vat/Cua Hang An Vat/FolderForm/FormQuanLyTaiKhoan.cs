﻿using Cua_Hang_An_Vat.Folder_Controller;
using Cua_Hang_An_Vat.FolderForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.GUI
{
    public partial class FormQuanLyTaiKhoan : Form
    {
        FormDangKiTaiKhoan formDangKiTaiKhoan;
        TaiKhoanController taiKhoanController;

        public FormQuanLyTaiKhoan()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            formDangKiTaiKhoan = new FormDangKiTaiKhoan();
            formDangKiTaiKhoan.FormQuanLyTaiKhoan = this;
          
            formDangKiTaiKhoan.Show();
        }

        public void refreshDataTaiKhoan()
        {
            taiKhoanController.Refresh();
        }

        private void khoiTao()
        {
            Program.khoiTaoControl(this);

        }
        private void FormQuanLyTaiKhoan_Load(object sender, EventArgs e)
        {
            khoiTao();
            taiKhoanController = new TaiKhoanController(dataTaiKhoan,txtTenTaiKhoan, txtTenNhanVien, dateNgaySinh, txtSoDienThoai, txtDiaChi,txtTimKiem, comboChucVu);
        }

        private void dataTaiKhoan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            taiKhoanController.hienThiThongTinKhiChonVaoDataGridView(e.RowIndex);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            taiKhoanController.suaTaiKhoan();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            taiKhoanController.xoaTaiKhoan();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            taiKhoanController.timKiemTaiKhoan();
        }
    }
}
