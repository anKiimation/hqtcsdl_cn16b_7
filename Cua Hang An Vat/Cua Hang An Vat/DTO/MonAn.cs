﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.DTO
{
    class MonAn
    {
        private String maSo;
        private String tenMonAn;
        private String nhomMonAn;
        private long giaTien;
        private bool khaDung;

   

        //set get
        public bool KhaDung
        {
            get { return khaDung; }
            set { khaDung = value; }
        }


        public String MaSo
        {
            get { return maSo; }
            set { maSo = value; }
        }
       

        public String TenMonAn
        {
            get { return tenMonAn; }
            set { tenMonAn = value; }
        }
        

        public String NhomMonAn
        {
            get { return nhomMonAn; }
            set { nhomMonAn = value; }
        }
       

        public long GiaTien
        {
            get { return giaTien; }
            set { giaTien = value; }
        }
        
    }
}
