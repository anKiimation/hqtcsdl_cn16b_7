﻿using Cua_Hang_An_Vat.DAL;
using Cua_Hang_An_Vat.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.Folder_Database
{
    class SQLGoiMon
    {
        private SQLConnect sqlConnect = new SQLConnect();

        public List<MonAn.MonAnDaGoi> dataTableToList(DataTable data)
        {
            List<MonAn.MonAnDaGoi> lstTemp = new List<MonAn.MonAnDaGoi>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                //String maSo = data.Rows[i]["MASO"].ToString();




                //bool banTrong = Convert.ToBoolean(data.Rows[i]["BANTRONG"].ToString());
                ////if (banTrongTemp == 1)
                ////    banTrong = true;


                //BanAn a = new BanAn(maSo, banTrong);



                //lstTemp.Add(a);
                //         private String maSo;
                //private String tenMonAn;
                //private String ;
                //private long giaTien;
                //private bool khaDung;

                string maSo = data.Rows[i]["MASO"].ToString();
                string tenMonAn = data.Rows[i]["TENMONAN"].ToString();
                long giaTien = Convert.ToInt64(data.Rows[i]["GIATIEN"].ToString());
                int soLuong = Convert.ToInt32(data.Rows[i]["SOLUONG"].ToString());

                MonAn.MonAnDaGoi a = new MonAn.MonAnDaGoi(maSo, tenMonAn,giaTien,soLuong);
                lstTemp.Add(a);

            }

            return lstTemp;
        }

        public DataTable loadDataGoiMon(string maBanAn, DateTime thoiGianVaoBan)
        {
            string query = string.Format("SELECT * FROM DANHSACHMONAN WHERE MABANAN = '{0}' AND THOIGIANVAOBAN = '{1}'", maBanAn, thoiGianVaoBan);
            return sqlConnect.executeQuery(query);
        }
        public DataTable loadDataGoiMonChiTiet(string maBanAn, DateTime thoiGianVaoBan)
        {
            string query = string.Format("SELECT MASO, TENMONAN, GIATIEN, SOLUONG = COUNT(MASO) FROM DANHSACHMONAN JOIN MONAN ON MAMONAN = MASO WHERE MABANAN = '{0}' AND THOIGIANVAOBAN = '{1}' GROUP BY MASO, TENMONAN,GIATIEN", maBanAn, thoiGianVaoBan);
            
         
            string queryTSQL = string.Format("exec xemDanhSachMonAn @mabanan = '{0}', @thoigianvaoban = '{1}'", maBanAn,thoiGianVaoBan);
            return sqlConnect.executeQuery(queryTSQL);
        }

        public List<MonAn.MonAnDaGoi> loadListGoiMon(string maBanAn, DateTime thoiGianVaoBan)
        {
            string query = string.Format("SELECT MASO, TENMONAN, GIATIEN, SOLUONG = COUNT(MASO) FROM DANHSACHMONAN JOIN MONAN ON MAMONAN = MASO WHERE MABANAN = '{0}' AND THOIGIANVAOBAN = '{1}' GROUP BY MASO, TENMONAN,GIATIEN", maBanAn, thoiGianVaoBan);
            return (dataTableToList(sqlConnect.executeQuery(query)));
        }
      

        public void themMonAn(string maBanAn, DateTime thoiGianVaoBan, string maMonAn)
        {
            /* update THONGTINBANAN 
set TONGSOMONAN = (select count(*) from DANHSACHMONAN where mabanan = 'ban20' and THOIGIANVAOBAN = '2019-04-17 17:27:00'),
tongtien = (select sum(giatien) from DANHSACHMONAN join MONAN on DANHSACHMONAN.MAMONAN = MONAN.MASO 
where mabanan = 'ban20' and THOIGIANVAOBAN = '2019-04-17 17:27:00')
where  maso = 'ban20' and THOIGIANVAOBAN = '2019-04-17 17:27:00'   */

            string query1 = string.Format("update THONGTINBANAN set TONGSOMONAN = (select count(*) from DANHSACHMONAN where mabanan = '{0}' and THOIGIANVAOBAN = '{1}'),tongtien = (select sum(giatien) from DANHSACHMONAN join MONAN on DANHSACHMONAN.MAMONAN = MONAN.MASO where mabanan = '{2}' and THOIGIANVAOBAN = '{3}')where  maso = '{4}' and THOIGIANVAOBAN = '{5}'", maBanAn, thoiGianVaoBan, maBanAn, thoiGianVaoBan,maBanAn,thoiGianVaoBan);
            string query2 = string.Format("INSERT INTO DANHSACHMONAN VALUES('{0}' ,'{1}' ,'{2}' )", maBanAn, thoiGianVaoBan, maMonAn);

            string queryTSQL = string.Format("exec dbo.themMonAn @maBanAN = '{0}', @thoiGianVaoBan = '{1}', @maMonAn='{2}'", maBanAn,thoiGianVaoBan,maMonAn);
            //sqlConnect.executeNonQuery(query1);
            //sqlConnect.executeNonQuery(query2);
            sqlConnect.executeNonQuery(queryTSQL);
        }

        public void xoaMonAn(string maBanAn, DateTime thoiGianVaoBan, string maMonAn)
        {
            string query = string.Format("DELETE FROM DANHSACHMONAN WHERE MABANAN = '{0}' AND THOIGIANVAOBAN = '{1}' AND MAMONAN = '{2}' ", maBanAn, thoiGianVaoBan, maMonAn);
            string query1 = string.Format("update THONGTINBANAN set TONGSOMONAN = (select count(*) from DANHSACHMONAN where mabanan = '{0}' and THOIGIANVAOBAN = '{1}'),tongtien = (select sum(giatien) from DANHSACHMONAN join MONAN on DANHSACHMONAN.MAMONAN = MONAN.MASO where mabanan = '{2}' and THOIGIANVAOBAN = '{3}')where  maso = '{4}' and THOIGIANVAOBAN = '{5}'", maBanAn, thoiGianVaoBan, maBanAn, thoiGianVaoBan, maBanAn, thoiGianVaoBan);
            //sqlConnect.executeNonQuery(query);
            //sqlConnect.executeNonQuery(query1);

            string queryTSQL = string.Format("exec dbo.xoaMonAn @maBanAN = '{0}', @thoiGianVaoBan = '{1}', @maMonAn='{2}' ", maBanAn,thoiGianVaoBan,maMonAn);
        }


       

    }
}
