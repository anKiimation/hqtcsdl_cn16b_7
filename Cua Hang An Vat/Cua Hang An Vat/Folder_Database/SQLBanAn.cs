﻿using Cua_Hang_An_Vat.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.DAL
{
    class SQLBanAn
    {
        private SQLConnect sqlConnect = new SQLConnect();

        public List<BanAn> dataTableToList(DataTable data)
        {
            List<BanAn> lstTemp = new List<BanAn>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                String maSo = data.Rows[i]["MASO"].ToString();
                bool banTrong = Convert.ToBoolean(  data.Rows[i]["BANTRONG"].ToString());
                bool anHien = Convert.ToBoolean(data.Rows[i]["ANHIEN"].ToString());
                //if (banTrongTemp == 1)
                //    banTrong = true;


                BanAn a = new BanAn(maSo, banTrong,anHien);

                lstTemp.Add(a);

            }

            return lstTemp;
        }

        public void hienBanAn(string maBanAn)
        {
            string query = string.Format("UPDATE BANAN SET ANHIEN = 1 WHERE MASO = '{0}'", maBanAn);
            sqlConnect.executeNonQuery(query);
        }
        public void anBanAn(string maBanAn)
        {
            string query = string.Format("UPDATE BANAN SET ANHIEN = 0 WHERE MASO = '{0}'", maBanAn);
            sqlConnect.executeNonQuery(query);
        }

        public void themBanAn()
        {
            string query = string.Format("INSERT INTO BANAN(MASO) VALUES('{0}')", getNewMaSo());
            sqlConnect.executeNonQuery(query);
        }
        public string getNewMaSo()
        {
            string query = "SELECT MASO FROM BANAN";
            DataTable data = sqlConnect.executeQuery(query);

            string maSo = (string)data.Rows[0][0];

            string maSoTemp2 = Regex.Replace(maSo, "[^0-9.]", "");

            int intMaSo = int.Parse(maSoTemp2);
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string maSo1 = (string)data.Rows[i][0];

                string maSoTemp21 = Regex.Replace(maSo1, "[^0-9.]", "");

                int intMaSo1 = int.Parse(maSoTemp21);

                if (intMaSo1 > intMaSo)
                    intMaSo = intMaSo1;

            }

            intMaSo++;
            string output = Regex.Replace(maSo, @"[\d-]", string.Empty);


            return output + intMaSo;
        }

        public List<BanAn> loadListBanAn()
        {
            String query = "SELECT * FROM BANAN WHERE ANHIEN=1";
            List<BanAn> lstBanAn = new List<BanAn>();
            lstBanAn  = this.dataTableToList(sqlConnect.executeQuery(query));
            return lstBanAn;
        }

        public List<BanAn> loadListBanAnFull()
        {
            String query = "SELECT * FROM BANAN";
            List<BanAn> lstBanAn = new List<BanAn>();
            lstBanAn = this.dataTableToList(sqlConnect.executeQuery(query));
            return lstBanAn;
        }

        public List<BanAn> loadListBanTrong()
        {
            String query = "SELECT * FROM BANAN WHERE BANTRONG = 1  AND ANHIEN=1";
            List<BanAn> lstBanAn = new List<BanAn>();
            lstBanAn = this.dataTableToList(sqlConnect.executeQuery(query));
            return lstBanAn;
        }

        public void chuyenBan(string maBanAn1, DateTime thoiGianVaoBan, string maBanAn2)
        {
            string queryTSQL = string.Format("exec dbo.chuyenBan @maBanAn1 = '{0}', @thoiGianVaoBan = '{1}', @maBanAn2 = '{2}'", maBanAn1,thoiGianVaoBan,maBanAn2);
            sqlConnect.executeNonQuery(queryTSQL);

        }

        public bool isBanTrong(string maSo)
        {
            String query = string.Format("SELECT * FROM BANAN where MASO = '{0}'  and ANHIEN=1", maSo);
            DataTable data = sqlConnect.executeQuery(query);
            bool banTrong = Convert.ToBoolean(data.Rows[0]["BANTRONG"].ToString());
            return banTrong;
        }

        public void ngoiVaoBan(String maSo)
        {
            String query1 = "UPDATE BANAN SET BANTRONG = 0 WHERE MASO = '" + maSo + "'";
            String query2 = "INSERT INTO THONGTINBANAN(MASO) VALUES ('" + maSo + "')";
            


                //sqlConnect.executeNonQuery(query1);
                //try
                //{
                //    sqlConnect.executeNonQuery(query2);
                //}
                //catch { MessageBox.Show("Vui lòng đợi...", "Thao tác nhanh!", MessageBoxButtons.OK, MessageBoxIcon.Stop); }
            string queryTSQL = string.Format("exec dbo.ngoiVaoBan @maBanAn = '{0}'",maSo);
            sqlConnect.executeNonQuery(queryTSQL);
        }

        public void traBanThemHoaDon(String maSo,DateTime thoiGianVaoBan, string tenTaiKhoan)
        {
            String query1 = "UPDATE BANAN SET BANTRONG = 1 WHERE MASO = '" + maSo + "'";
            String query2 = string.Format("UPDATE THONGTINBANAN SET TENTAIKHOAN = '{0}' WHERE MASO = '{1}' AND THOIGIANVAOBAN = '{2}' ", tenTaiKhoan, maSo, thoiGianVaoBan);

            //sqlConnect.executeNonQuery(query1);
            //sqlConnect.executeNonQuery(query2);

            String queryTSQL = string.Format("exec dbo.traBanThemHoaDon @maBanAn = '{0}', @thoiGianVaoBan = '{1}', @tenTaiKhoan = '{2}'", maSo,thoiGianVaoBan,tenTaiKhoan);
            sqlConnect.executeNonQuery(queryTSQL);

        }

        public void traBan(String maSo, DateTime thoiGianVaoBan, string tenTaiKhoan)
        {
            String query1 = "UPDATE BANAN SET BANTRONG = 1 WHERE MASO = '" + maSo + "'";
            String query2 = string.Format("DELETE FROM THONGTINBANAN WHERE MASO = '{0}' AND THOIGIANVAOBAN = '{1}' ",maSo, thoiGianVaoBan);

            //sqlConnect.executeNonQuery(query1);
            //sqlConnect.executeNonQuery(query2);
            String queryTSQL = string.Format("exec dbo.traBan @maBanAn = '{0}', @thoiGianVaoBan = '{1}' ", maSo, thoiGianVaoBan);
            sqlConnect.executeNonQuery(queryTSQL);
        }

        public bool hienThiThongTinBanAn(ref String maSo, ref DateTime thoiGianVaoBan,ref  int tongSoMonAn, ref long khuyenMai, ref long TongTien)
        {
            String query = string.Format("select top 1 * from THONGTINBANAN join banan on BANAN.MASO = THONGTINBANAN.MASO where banan.maso = '{0}' and bantrong = 0 order by THOIGIANVAOBAN desc", maSo);
            DataTable data = sqlConnect.executeQuery(query);
            try
            {
                thoiGianVaoBan = Convert.ToDateTime(data.Rows[0]["THOIGIANVAOBAN"].ToString());
                tongSoMonAn = Convert.ToInt32(data.Rows[0]["TONGSOMONAN"].ToString());
                TongTien = Convert.ToInt64(data.Rows[0]["tongtien"].ToString());
                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }

        public int tongSoMonAn(String maSo)
        {
            String query = string.Format("select top 1 * from THONGTINBANAN join banan on BANAN.MASO = THONGTINBANAN.MASO where banan.maso = '{0}' and bantrong = 0 order by THOIGIANVAOBAN desc", maSo);
            DataTable data = sqlConnect.executeQuery(query);
            try
            {
                
                return Convert.ToInt32(data.Rows[0]["TONGSOMONAN"].ToString());

            }
            catch
            {
                return 0;
            }
        }

    }
}
