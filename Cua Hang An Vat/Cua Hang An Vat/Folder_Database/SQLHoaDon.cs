﻿using Cua_Hang_An_Vat.DAL;
using Cua_Hang_An_Vat.Folder_Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.Folder_Database
{
    class SQLHoaDon
    {
        private SQLConnect sqlConnect = new SQLConnect();

        public DataTable loadHoaDon()
        {
            return sqlConnect.executeQuery("select * from THONGTINBANAN where TENTAIKHOAN is not null");
        }

      
        public DataTable timKiemHoaDon(string timKiem)
        {
            return sqlConnect.executeQuery(string.Format("SELECT * FROM THONGTINBANAN where maso LIKE '%{0}%' or thoigianvaoban LIKE '%{1}%' or tongtien LIKE '%{2}%'", timKiem,timKiem,timKiem));
        }

        internal Folder_Class.HoaDon hoaDonDaChon(string maBanAn, string thoiGianVaoBan)
        {
            string query = string.Format("SELECT * FROM THONGTINBANAN WHERE MASO='{0}' AND THOIGIANVAOBAN = '{1}'", maBanAn, thoiGianVaoBan);
            DataTable data = sqlConnect.executeQuery(query);
            string maSo = maBanAn; //0
            int tongSoMonAn =Convert.ToInt32( data.Rows[0][2].ToString()); //2
            long tongTien = Convert.ToInt32(data.Rows[0][3].ToString());
            string tenTaiKhoan = data.Rows[0][4].ToString();

            HoaDon hoaDon = new HoaDon(maBanAn, thoiGianVaoBan, tongSoMonAn, tongTien, tenTaiKhoan);
            return hoaDon;
        }


  
    }
}
