﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.DAL
{
    class SQLConnect
    {
        string strSQLConnect = "Data Source=ANKIIMATION;Initial Catalog=QUANLYCUAHANGANVAT;Integrated Security=True";

        public string StrSQLConnect
        {
            get { return strSQLConnect; }
            set { strSQLConnect = value; }
        }

        public DataTable executeQuery(string query)
        {
            try
            {
                DataTable data = new DataTable();
                SqlConnection sql = new SqlConnection(strSQLConnect);
                sql.InfoMessage += sql_InfoMessage;
                sql.Open();
                SqlCommand command = new SqlCommand(query, sql);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(data);
                sql.Close();
                return data;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(strSQLMessage);

                return null;
            }
        }


        public void executeNonQuery(string query)
        {
            try
            {
                SqlConnection sql = new SqlConnection(strSQLConnect);
                sql.InfoMessage += sql_InfoMessage;
                sql.Open();
                SqlCommand command = new SqlCommand(query, sql);
                command.ExecuteNonQuery();
                sql.Close();
             
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(strSQLMessage);
                MessageBox.Show(e.Message, "Xảy ra lỗi!",MessageBoxButtons.OK, MessageBoxIcon.Stop);
                
            }





        }

        void sql_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            strSQLMessage = e.Message;
            //MessageBox.Show(this.strSQLMessage, "Thong bao sql");
        }


        private static string strSQLMessage;

        public static string StrSQLMessage
        {
            get { return strSQLMessage; }
            set { strSQLMessage = value; }
        }
    }
}


