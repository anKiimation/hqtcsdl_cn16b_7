﻿using Cua_Hang_An_Vat.DAL;
using Cua_Hang_An_Vat.Folder_Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.Folder_Database
{
    class SQLTaiKhoan
    {
        private SQLConnect sqlConnect = new SQLConnect();

        public List<TaiKhoan> dataTableToList(DataTable data)
        {
            List<TaiKhoan> lstTemp = new List<TaiKhoan>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
     

                string tenTaiKhoan = data.Rows[i]["TENTAIKHOAN"].ToString();
                string matKhau = data.Rows[i]["MATKHAU"].ToString();
                string tenNhanVien = data.Rows[i]["TENNHANVIEN"].ToString();
                DateTime ngaySinh = Convert.ToDateTime(data.Rows[i]["NGAYSINH"].ToString());
                string soDienThoai = data.Rows[i]["SODIENTHOAI"].ToString();
                string diaChi = data.Rows[i]["DIACHI"].ToString();

                TaiKhoan a = new TaiKhoan(tenTaiKhoan, matKhau, tenNhanVien, ngaySinh, soDienThoai, diaChi);
                lstTemp.Add(a);

            }

            return lstTemp;
        }


        public DataTable loadDataTaiKhoan()
        {
            String query = "SELECT * FROM TaiKhoan";
            return sqlConnect.executeQuery(query);
        }

        public List<TaiKhoan> loadListTaiKhoan()
        {
            String query = "SELECT * FROM TaiKhoan";
            List<TaiKhoan> lstBanAn = new List<TaiKhoan>();
            lstBanAn = this.dataTableToList(sqlConnect.executeQuery(query));
            return lstBanAn;
        }

        public bool kiemTraTaiKhoan(string tenTaiKhoan, string matKhau)
        {
            String query =string.Format( "SELECT tentaikhoan FROM TaiKhoan where tentaikhoan = '{0}' and matkhau = '{1}'", tenTaiKhoan, matKhau);
            DataTable data = sqlConnect.executeQuery(query);
            if (data.Rows.Count <= 0)
            {
                return false;
            }
           
                return true;
            
        }

        public bool kiemTraAdmin(string tenTaiKhoan, string matKhau)
        {
            string query = string.Format("SELECT ISADMIN FROM TAIKHOAN WHERE tentaikhoan = '{0}' and matkhau = '{1}'", tenTaiKhoan, matKhau);
            DataTable data = sqlConnect.executeQuery(query);
            bool isAdmin = Convert.ToBoolean(data.Rows[0][0].ToString());
            return isAdmin;
        }

        public TaiKhoan thongTinTaiKhoan(string tenTaiKhoan, string matKhau)
        {
            if (kiemTraTaiKhoan(tenTaiKhoan, matKhau))
            {
                string query = string.Format("SELECT TENNHANVIEN,NGAYSINH,SODIENTHOAI,DIACHI FROM TAIKHOAN WHERE tentaikhoan = '{0}' and matkhau = '{1}'", tenTaiKhoan, matKhau);
                DataTable data = sqlConnect.executeQuery(query);
                string tenNhanVien = data.Rows[0][0].ToString();
                DateTime ngaySinh = Convert.ToDateTime(data.Rows[0][1].ToString());
                string soDienThoai = data.Rows[0][2].ToString();
                string diaChi = data.Rows[0][3].ToString();

                TaiKhoan taiKhoan = new TaiKhoan(tenTaiKhoan, matKhau, tenNhanVien, ngaySinh, soDienThoai, diaChi);
                return taiKhoan;
            }
            else
                return null;

        }


        public void themTaiKhoan(string tenDangNhap, string matKhau, string tenNhanVien, string ngaySinh, string soDienThoai, string diaChi)
        {
            string query = string.Format("INSERT INTO TAIKHOAN VALUES('{0}','{1}',N'{2}','{3}','{4}',N'{5}',0)", tenDangNhap,matKhau,tenNhanVien,ngaySinh,soDienThoai,diaChi);
            sqlConnect.executeNonQuery(query);
        }

        public void xoaTaiKhoan(string tenTaiKhoan)
        {
            string query = string.Format("DELETE FROM TAIKHOAN WHERE TENTAIKHOAN = '{0}'", tenTaiKhoan);
            sqlConnect.executeNonQuery(query);
        }

        public void suaTaiKhoan(string tenTaiKhoan, string tenNhanVien, string ngaySinh, string soDienThoai, string diaChi)
        {
            string query = string.Format("UPDATE TAIKHOAN SET TENNHANVIEN = N'{0}', NGAYSINH = '{1}', SODIENTHOAI = '{2}', DIACHI = N'{3}' WHERE TENTAIKHOAN = '{4}'  ", tenNhanVien, ngaySinh, soDienThoai, diaChi, tenTaiKhoan);
            sqlConnect.executeNonQuery(query);
            
        }

        public void doiMatKhau(string tenTaiKhoan,string matkhauMoi)
        {
            string query = string.Format("UPDATE TAIKHOAN SET MATKHAU = '{0}' WHERE TENTAIKHOAN = '{1}' ", matkhauMoi, tenTaiKhoan);
            sqlConnect.executeNonQuery(query);
        }

        public DataTable timKiemTaiKhoan(string keyword)
        {
            string query = string.Format("SELECT * FROM TAIKHOAN WHERE TENTAIKHOAN LIKE '%{0}%' OR TENNHANVIEN LIKE '%{1}%' or  SODIENTHOAI LIKE '%{2}%'", keyword, keyword, keyword);
            return sqlConnect.executeQuery(query);
        }

        internal void suaTaiKhoan(string tenDangNhap, string tenNhanVien, string ngaySinh, string soDienThoai, string diaChi, bool isAdmin)
        {
            int intAdmin = 0;
            if (isAdmin)
            {
                intAdmin = 1;
            }

            string query = string.Format("UPDATE TAIKHOAN SET TENNHANVIEN = N'{0}', NGAYSINH = '{1}', SODIENTHOAI = '{2}', DIACHI = N'{3}', isadmin = {4} WHERE TENTAIKHOAN = '{5}'  ", tenNhanVien, ngaySinh, soDienThoai, diaChi,intAdmin, tenDangNhap);
            sqlConnect.executeNonQuery(query);
        }
    }
}
