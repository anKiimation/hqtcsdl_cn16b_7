﻿using Cua_Hang_An_Vat.DAL;
using Cua_Hang_An_Vat.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.Folder_Database
{
    class SQLMonAn
    {
        private SQLConnect sqlConnect = new SQLConnect();

        public List<MonAn> dataTableToList(DataTable data)
        {
            List<MonAn> lstTemp = new List<MonAn>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                //String maSo = data.Rows[i]["MASO"].ToString();




                //bool banTrong = Convert.ToBoolean(data.Rows[i]["BANTRONG"].ToString());
                ////if (banTrongTemp == 1)
                ////    banTrong = true;


                //BanAn a = new BanAn(maSo, banTrong);



                //lstTemp.Add(a);
        //         private String maSo;
        //private String tenMonAn;
        //private String nhomMonAn;
        //private long giaTien;
        //private bool khaDung;

                string maSo = data.Rows[i]["MASO"].ToString();
                string tenMonAn = data.Rows[i]["TENMONAN"].ToString();
                long giaTien = Convert.ToInt64(data.Rows[i]["GIATIEN"].ToString());
                bool khaDung = Convert.ToBoolean(data.Rows[i]["KHADUNG"].ToString());

                MonAn a = new MonAn(maSo, tenMonAn, giaTien, khaDung);
                lstTemp.Add(a);

            }

            return lstTemp;
        }


        public DataTable loadDataMonAn()
        {
            String query = "SELECT * FROM MONAN";
            return sqlConnect.executeQuery(query);
        }

        public DataTable loadDataMonAnKhaDung()
        {
            String query = "SELECT * FROM MONAN WHERE KHADUNG = 1";
            return sqlConnect.executeQuery(query);
        }

        public List<MonAn> loadListMonAn()
        {
            String query = "SELECT * FROM MONAN";
            List<MonAn> lstBanAn = new List<MonAn>();
            lstBanAn = this.dataTableToList(sqlConnect.executeQuery(query));
            return lstBanAn;
        }

        public string getNewMaSo()
        {
            string query = "SELECT MASO FROM MONAN";
            DataTable data = sqlConnect.executeQuery(query);

            string maSo = (string)data.Rows[0][0];

            string maSoTemp2 = Regex.Replace(maSo, "[^0-9.]", "");

            int intMaSo = int.Parse(maSoTemp2);
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string maSo1 = (string)data.Rows[i][0];

                string maSoTemp21 = Regex.Replace(maSo1, "[^0-9.]", "");

                int intMaSo1 = int.Parse(maSoTemp21);

                if (intMaSo1 > intMaSo)
                    intMaSo = intMaSo1 ;
                
            }
           
            intMaSo++;
            string  output = Regex.Replace(maSo, @"[\d-]", string.Empty);


            return output + intMaSo;
        }

        //public void themMonAn(string maSo, string tenMonAn, string nhomMonAn, long giaTien, bool khaDung)
        //{
        //    int intKhaDung = 0;
        //    if (khaDung)
        //    {
        //        intKhaDung = 1;
        //    }
        //    string query =string.Format( "insert into MONAN VALUES ('{0}', N'{1}', N'{2}', {3}, {4})", maSo,tenMonAn,nhomMonAn,giaTien,intKhaDung);
        //    sqlConnect.executeNonQuery(query);
        //}

        public void themMonAn(MonAn monAn) {
            int intKhaDung = 0;
            if (monAn.KhaDung)
            {
                intKhaDung = 1;
            }
            string query = string.Format("insert into MONAN VALUES ('{0}', N'{1}', {2}, {3})", monAn.MaSo, monAn.TenMonAn, monAn.GiaTien, intKhaDung);
            sqlConnect.executeNonQuery(query);
        
        }

        public void xoaMonAn(string maSo)
        {
            string query = string.Format("DELETE FROM MONAN WHERE MASO = '{0}'", maSo);
            sqlConnect.executeNonQuery(query);
        }
        public void suaMonAn(string maSo,string tenMonAn, long giaTien, bool khaDung)
        {
            int intKhaDung = 0;
            if (khaDung)
            {
                intKhaDung = 1;
            }
            string query = string.Format("UPDATE MONAN SET TENMONAN = N'{0}' , GIATIEN = {1}, KHADUNG = {2} WHERE MASO = '{3}' ", tenMonAn, giaTien, intKhaDung, maSo);
            sqlConnect.executeNonQuery(query);
        }



        public DataTable timKiemMonAn(string keyword)
        {
            string query = string.Format("SELECT * FROM MONAN WHERE MASO LIKE '%{0}%' OR TENMONAN LIKE '%{1}%' or  GIATIEN LIKE '%{2}%'", keyword, keyword, keyword);
           return sqlConnect.executeQuery(query);
        }
    }
}
