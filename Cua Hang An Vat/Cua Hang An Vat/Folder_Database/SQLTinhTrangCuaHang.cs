﻿using Cua_Hang_An_Vat.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.Folder_Database
{
    class SQLTinhTrangCuaHang
    {
        SQLConnect sqlConnect = new SQLConnect();

        public long tongDoanhThuTheoNam(int nam)
        {
            string query = string.Format("SELECT SUM(TONGTIEN) AS 'Tổng doanh thu theo năm' FROM THONGTINBANAN WHERE year(thoigianvaoban) = {0}", nam);
            DataTable data = sqlConnect.executeQuery(query);
            long tongDoanhThu = 0;
            try
            {
                tongDoanhThu = Convert.ToInt64(data.Rows[0][0].ToString());
            }
            catch { }
            return tongDoanhThu;
        }
        public long tongDoanhThuTheoThang(int thang, int nam)
        {
            string query = string.Format("SELECT SUM(TONGTIEN) AS 'Tổng doanh thu theo tháng' FROM THONGTINBANAN WHERE month(thoigianvaoban)= {0} and year(thoigianvaoban) = {1}", thang,nam);
            DataTable data = sqlConnect.executeQuery(query);
            long tongDoanhThu = 0;
            try
            {
                tongDoanhThu = Convert.ToInt64(data.Rows[0][0].ToString());
            }
            catch { }
            return tongDoanhThu;
        }
        public long tongDoanhThuTheoNgay(int ngay,int thang, int nam)
        {
            string query = string.Format("SELECT SUM(TONGTIEN) AS 'Tổng doanh thu theo ngày' FROM THONGTINBANAN WHERE day(thoigianvaoban)= {0} and month(thoigianvaoban) = {1} and year(thoigianvaoban) = {2}", ngay, thang, nam);
            DataTable data = sqlConnect.executeQuery(query);
            long tongDoanhThu = 0;
            try
            {
              tongDoanhThu   = Convert.ToInt64(data.Rows[0][0].ToString());
            }
            catch { }
            return tongDoanhThu;
        }
        //mon an
        public DataTable monAnGoiNhieuNhatTheoNgay(int ngay, int thang, int nam)
        {
            string query = string.Format("SELECT TENMONAN, GIATIEN, COUNT(MAMONAN) AS SOLANGOI FROM MONAN JOIN DANHSACHMONAN ON DANHSACHMONAN.MAMONAN = MONAN.MASO where DAY(THOIGIANVAOBAN) = {0}  and MONTH(THOIGIANVAOBAN) = {1} and year(thoigianvaoban) = {2} GROUP BY TENMONAN, GIATIEN ORDER BY SOLANGOI DESC", ngay, thang, nam);
            return sqlConnect.executeQuery(query);
        }
        public DataTable monAnGoiNhieuNhatTheoThang(int thang, int nam)
        {
            string query = string.Format("SELECT TENMONAN, GIATIEN, COUNT(MAMONAN) AS SOLANGOI FROM MONAN JOIN DANHSACHMONAN ON DANHSACHMONAN.MAMONAN = MONAN.MASO where MONTH(THOIGIANVAOBAN) = {0} and year(thoigianvaoban) = {1} GROUP BY TENMONAN, GIATIEN ORDER BY SOLANGOI DESC",thang, nam);
            return sqlConnect.executeQuery(query);
        }
        public DataTable monAnGoiNhieuNhatTheoNam(int nam)
        {
            string query = string.Format("SELECT TENMONAN, GIATIEN, COUNT(MAMONAN) AS SOLANGOI FROM MONAN JOIN DANHSACHMONAN ON DANHSACHMONAN.MAMONAN = MONAN.MASO where year(thoigianvaoban) = {0} GROUP BY TENMONAN, GIATIEN ORDER BY SOLANGOI DESC", nam);
            return sqlConnect.executeQuery(query);
        }
        //ban an ngoi nhieu
        public DataTable banAnNgoiNhieuNhatNgay(int ngay, int thang, int nam)
        {
            string query = string.Format("SELECT maso,COUNT(MASO) AS SOLANNGOI FROM  THONGTINBANAN where  DAY(THONGTINBANAN.THOIGIANVAOBAN) = {0} and MONTH(THONGTINBANAN.THOIGIANVAOBAN) = {1} and year(THONGTINBANAN.THOIGIANVAOBAN) = {2} GROUP BY MASO ORDER BY SOLANNGOI DESC",ngay,thang,nam);
            return sqlConnect.executeQuery(query);
        }

        public DataTable banAnNgoiNhieuNhatThang(int thang, int nam)
        {
            string query = string.Format("SELECT maso,COUNT(MASO) AS SOLANNGOI FROM  THONGTINBANAN where  MONTH(THONGTINBANAN.THOIGIANVAOBAN) = {0} and year(THONGTINBANAN.THOIGIANVAOBAN) = {1} GROUP BY MASO ORDER BY SOLANNGOI DESC", thang, nam);
            return sqlConnect.executeQuery(query);
        }

        public DataTable banAnNgoiNhieuNhatNam(int nam)
        {
            string query = string.Format("SELECT maso,COUNT(MASO) AS SOLANNGOI FROM  THONGTINBANAN where  year(THONGTINBANAN.THOIGIANVAOBAN) = {0} GROUP BY MASO ORDER BY SOLANNGOI DESC", nam);
            return sqlConnect.executeQuery(query);
        }
        //ban an doanh thu
        public DataTable banAnCoDoanhThuNhieuNhatNgay(int ngay,int thang, int nam){
         string query = string.Format("SELECT MASO, SUM(TONGTIEN) AS TONGTIEN FROM THONGTINBANAN WHERE DAY(THOIGIANVAOBAN) = {0}  and MONTH(THOIGIANVAOBAN) = {1} and year(thoigianvaoban) = {2} GROUP BY MASO ORDER BY TONGTIEN DESC",ngay,thang,nam);
            return sqlConnect.executeQuery(query);

        }
        public DataTable banAnCoDoanhThuNhieuNhatThang(int thang, int nam)
        {
            string query = string.Format("SELECT MASO, SUM(TONGTIEN) AS TONGTIEN FROM THONGTINBANAN WHERE MONTH(THOIGIANVAOBAN) = {0} and year(thoigianvaoban) = {1} GROUP BY MASO ORDER BY TONGTIEN DESC", thang, nam);
            return sqlConnect.executeQuery(query);

        }

        public DataTable banAnCoDoanhThuNhieuNhatNam( int nam)
        {
            string query = string.Format("SELECT MASO, SUM(TONGTIEN) AS TONGTIEN FROM THONGTINBANAN WHERE year(thoigianvaoban) = {0} GROUP BY MASO ORDER BY TONGTIEN DESC", nam);
            return sqlConnect.executeQuery(query);

        }




        ///////

        public List<int> loadNam()
        {
            List<int> lstTemp = new List<int>();
            string query = "SELECT distinct year(thoigianvaoban) FROM THONGTINBANAN";
            DataTable data = sqlConnect.executeQuery(query);
            for (int i = 0; i < data.Rows.Count; i++) { 
                lstTemp.Add(Convert.ToInt32( data.Rows[i][0].ToString()));
            }
            return lstTemp;
        }


    }
}
