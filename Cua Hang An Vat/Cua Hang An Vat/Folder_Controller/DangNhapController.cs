﻿using Cua_Hang_An_Vat.Folder_Class;
using Cua_Hang_An_Vat.Folder_Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.Folder_Controller
{
    class DangNhapController
    {
        SQLTaiKhoan sqlTaiKhoan = new SQLTaiKhoan();

        TextBox txtTenTaiKhoan, txtMatKhau;
        public DangNhapController(TextBox txtTenTaiKhoan, TextBox txtMatKhau)
        {
            this.txtTenTaiKhoan = txtTenTaiKhoan;
            this.txtMatKhau = txtMatKhau;
        }

        public DangNhapController()
        {
            // TODO: Complete member initialization
        }

        public string taoMatKhau(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        //chuc nang
        public TaiKhoan dangNhap()
        {
            TaiKhoan taiKhoan = sqlTaiKhoan.thongTinTaiKhoan(txtTenTaiKhoan.Text, taoMatKhau(txtMatKhau.Text));
            if (taiKhoan != null)
            {
                if (sqlTaiKhoan.kiemTraAdmin(txtTenTaiKhoan.Text, taoMatKhau(txtMatKhau.Text)))
                    taiKhoan.IsAdmin = true;
                return taiKhoan;
            }
            return null;
        }

        public bool dangNhap(string tenTaiKhoan,string matKhau)
        {
            TaiKhoan taiKhoan = sqlTaiKhoan.thongTinTaiKhoan(tenTaiKhoan, taoMatKhau(matKhau));
            if (taiKhoan != null)
            {
                if (sqlTaiKhoan.kiemTraAdmin(tenTaiKhoan, taoMatKhau(matKhau)))
                    taiKhoan.IsAdmin = true;
                return true;
            }
            return false ;
        }
        
    }
}
