﻿using Cua_Hang_An_Vat.DTO;
using Cua_Hang_An_Vat.Folder_Class;
using Cua_Hang_An_Vat.Folder_Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.Folder_Controller
{
    class HoaDonController
    {
        SQLHoaDon sqlHoaDon = new SQLHoaDon();

        private System.Windows.Forms.DataGridView dataDanhSachHoaDon;
        private System.Windows.Forms.TextBox txtTimKiem;
        private System.Windows.Forms.TextBox txtBan;
        private System.Windows.Forms.TextBox txtNhanVien;
        private System.Windows.Forms.TextBox txtThoiGianVaoBan;
        private System.Windows.Forms.TextBox txtTongSoMonAn;
        private System.Windows.Forms.TextBox txtTongTien;
        private System.Windows.Forms.ListView listMonAn;

        public HoaDonController(System.Windows.Forms.DataGridView dataDanhSachHoaDon, System.Windows.Forms.TextBox txtTimKiem, System.Windows.Forms.TextBox txtBan, System.Windows.Forms.TextBox txtNhanVien, System.Windows.Forms.TextBox txtThoiGianVaoBan, System.Windows.Forms.TextBox txtTongSoMonAn, System.Windows.Forms.TextBox txtTongTien, System.Windows.Forms.ListView listMonAn)
        {
            // TODO: Complete member initialization
            this.dataDanhSachHoaDon = dataDanhSachHoaDon;
            this.txtTimKiem = txtTimKiem;
            this.txtBan = txtBan;
            this.txtNhanVien = txtNhanVien;
            this.txtThoiGianVaoBan = txtThoiGianVaoBan;
            this.txtTongSoMonAn = txtTongSoMonAn;
            this.txtTongTien = txtTongTien;
            this.listMonAn = listMonAn;
            refresh();
        }

        private void refresh()
        {
            this.dataDanhSachHoaDon.DataSource = sqlHoaDon.loadHoaDon();

            this.dataDanhSachHoaDon.Columns["MASO"].HeaderText = "Mã bàn ăn";
            this.dataDanhSachHoaDon.Columns["THOIGIANVAOBAN"].HeaderText = "Thời gian vào bàn";
            this.dataDanhSachHoaDon.Columns["TONGTIEN"].HeaderText = "Tổng tiền";
            this.dataDanhSachHoaDon.Columns["TONGSOMONAN"].HeaderText = "Tổng số món ăn";
            this.dataDanhSachHoaDon.Columns["TENTAIKHOAN"].Visible = false;

        }


        /*
          khoiTao();
            txtNhanVien.Text = tenNhanVien;
            txtBan.Text = maBanAn;
            txtTongSoMonAn.Text = tongSoMonAn +"";
            txtTongTien.Text = tongTien+"";
            txtThoiGianVaoBan.Text = thoiGianVao;
            this.Text = maBanAn;
            List<MonAn.MonAnDaGoi> lst = (new SQLGoiMon()).loadListGoiMon(maBanAn, Convert.ToDateTime(thoiGianVao));
            foreach (MonAn.MonAnDaGoi i in lst)
            {
                string[] row = {i.TenMonAn, i.GiaTien.ToString(), i.SoLuong.ToString()};
                ListViewItem a = new ListViewItem(row);
                listMonAn.Items.Add(a);
            }
            listMonAn.Columns[0].Width = -2;
         
         */

        public void hienThiThongTinHoaDon(int index)
        {
            
            listMonAn.Items.Clear();
           

            if (index >= 0)
            {
               

                HoaDon hoaDonDaChon;

                dataDanhSachHoaDon.Rows[index].Selected = true;

                DataGridViewRow row = this.dataDanhSachHoaDon.Rows[index];

                string maBanAn = row.Cells[0].Value.ToString();
                string thoiGianVaoBan = row.Cells[1].Value.ToString();

                hoaDonDaChon = sqlHoaDon.hoaDonDaChon(maBanAn, thoiGianVaoBan);

                txtNhanVien.Text = hoaDonDaChon.TenTaiKhoan;
                txtBan.Text = hoaDonDaChon.MaBanAn;
                txtTongSoMonAn.Text = hoaDonDaChon.TongSoMonAn + "";
                txtTongTien.Text = hoaDonDaChon.TongTien + "";
                txtThoiGianVaoBan.Text = hoaDonDaChon.ThoiGianVaoBan;

                List<MonAn.MonAnDaGoi> lst = (new SQLGoiMon()).loadListGoiMon(maBanAn, Convert.ToDateTime(hoaDonDaChon.ThoiGianVaoBan));
                foreach (MonAn.MonAnDaGoi i in lst)
                {
                    string[] rowList = { i.TenMonAn, i.GiaTien.ToString(), i.SoLuong.ToString() };
                    ListViewItem a = new ListViewItem(rowList);
                    listMonAn.Items.Add(a);
                }
                
                    listMonAn.Columns[0].Width = -2;
              
                

            }

        }

        internal void timKiemHoaDon()
        {
            dataDanhSachHoaDon.DataSource = sqlHoaDon.timKiemHoaDon(txtTimKiem.Text);
        }
    }
}
