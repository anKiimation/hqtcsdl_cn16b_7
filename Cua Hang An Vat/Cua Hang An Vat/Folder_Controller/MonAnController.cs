﻿using Cua_Hang_An_Vat.DTO;
using Cua_Hang_An_Vat.Folder_Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.Folder_Controller
{
    class MonAnController
    {
        SQLMonAn sqlMonAn = new SQLMonAn();
        TextBox txtMaSo, txtTenMonAn, txtGiaTien, txtTimKiem;
        ComboBox comboKhaDung;
        DataGridView dataMonAn;

        public MonAnController(DataGridView dataMonAn, TextBox txtMaSo, TextBox txtTenMonAn, TextBox txtGiaTien, ComboBox comboKhaDung, TextBox txtTimKiem)
        {
            this.dataMonAn=dataMonAn;
            this.txtMaSo = txtMaSo;
            this.txtTenMonAn=txtTenMonAn;

            this.txtGiaTien=txtGiaTien;
            this.txtTimKiem = txtTimKiem;
            this.comboKhaDung = comboKhaDung;




            refresh();
            txtMaSo.Text = sqlMonAn.getNewMaSo();

        }
        private void refresh() {
            dataMonAn.DataSource = sqlMonAn.loadDataMonAn();
            dataMonAn.Columns["MASO"].HeaderText = "Mã số";
            dataMonAn.Columns["TENMONAN"].HeaderText = "Tên món ăn";
            dataMonAn.Columns["GIATIEN"].HeaderText = "Giá tiền";
            dataMonAn.Columns["KHADUNG"].HeaderText = "Khả dụng";
        }

        public MonAnController(TextBox txtMaSo, TextBox txtTenMonAn,  TextBox txtGiaTien, ComboBox comboKhaDung)
        {
            // TODO: Complete member initialization
            this.txtMaSo = txtMaSo;
            this.txtTenMonAn = txtTenMonAn;

            this.txtGiaTien = txtGiaTien;
            this.comboKhaDung = comboKhaDung;
            txtMaSo.Text = sqlMonAn.getNewMaSo();
        }

        public void Refresh()
        {
            dataMonAn.DataSource = (new SQLMonAn()).loadDataMonAn();
            txtMaSo.Text = sqlMonAn.getNewMaSo();
        }

        public void clearTextBox()
        {
            txtTenMonAn.Text = "";

            txtGiaTien.Text = "0";
            
        }

        public void thongBao(bool kq)
        {
            if (kq)
                MessageBox.Show("Thực hiện thành công!", "Thành công", MessageBoxButtons.OK,MessageBoxIcon.Information);
            else
            {
                MessageBox.Show("Thực hiện không thành công!", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void thongBao(Exception e)
        {
            
                MessageBox.Show("Thực hiện không thành công!\n" + "Lỗi: " + e.Message, "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            
        }






        public void hienThiThongTinKhiChonVaoDataGridView(int index)
        {
            if (index >= 0)
            {
                dataMonAn.Rows[index].Selected = true;

                DataGridViewRow row = this.dataMonAn.Rows[index];
                txtMaSo.Text = row.Cells[0].Value.ToString();
                txtTenMonAn.Text = row.Cells[1].Value.ToString();
               
                txtGiaTien.Text = row.Cells[2].Value.ToString();

                bool khaDung =Convert.ToBoolean( row.Cells[3].Value.ToString());
                if (khaDung)
                {
                    comboKhaDung.SelectedIndex = comboKhaDung.FindString("Hiện");
                }
                else
                {
                    comboKhaDung.SelectedIndex = comboKhaDung.FindString("Ẩn");
                }
            }
        }

        public void unselect()
        {
            dataMonAn.ClearSelection();
           
            clearTextBox();
            Refresh();
        }

        //chuc nang
        public void themMonAn()
        {

            try
            {
                string maSo = txtMaSo.Text;

                string tenMonAn = txtTenMonAn.Text;
                //string nhomMonAn = txtNhomMonAn.Text;
                long giaTien = Convert.ToInt64(txtGiaTien.Text.Replace(",", ""));

                string stringKhaDung = comboKhaDung.SelectedItem.ToString();
                bool khaDung = true;
                if (stringKhaDung.Equals("Hiện"))
                {
                     khaDung = true;
                }
                else
                {
                     khaDung = false ;
                }

                MonAn monAn = new MonAn(maSo, tenMonAn, giaTien, khaDung);

                sqlMonAn.themMonAn(monAn);
                thongBao(true);

                //Refresh();
                clearTextBox();
            }
            catch (Exception e)
            {
                thongBao(e);
            }
        }

        public void xoaMonAn()
        {
            try
            {
                string maSo = txtMaSo.Text;
                sqlMonAn.xoaMonAn(maSo);
                thongBao(true);

                Refresh();
                clearTextBox();
            }
            catch (Exception e)
            {
                thongBao(e);
            }
        }

        public void capNhatMonAn()
        {
            try
            {
                string maSo = txtMaSo.Text;

                string tenMonAn = txtTenMonAn.Text;
                long giaTien = Convert.ToInt64(txtGiaTien.Text.Replace(",",""));
                string stringKhaDung = comboKhaDung.SelectedItem.ToString();
                bool khaDung = true;
                if (stringKhaDung.Equals("Hiện"))
                {
                     khaDung = true;
                }
                else
                {
                     khaDung = false;
                }


                sqlMonAn.suaMonAn(maSo, tenMonAn, giaTien, khaDung);
                thongBao(true);

                Refresh();
                clearTextBox();
            }
            catch(Exception e)
            {
                thongBao(e);
            }
        }

        public void timKiemMonAn()
        {
            dataMonAn.DataSource =  sqlMonAn.timKiemMonAn(txtTimKiem.Text);
            
        }


    }
}
