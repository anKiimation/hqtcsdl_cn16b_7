﻿using Cua_Hang_An_Vat.DAL;
using Cua_Hang_An_Vat.DTO;
using Cua_Hang_An_Vat.Folder_Controller;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.BUS
{
    class BanAnController
    {

        SQLBanAn sqlBanAn = new SQLBanAn();
        TextBox txtMaSo, txtThoiGianVaoBan,txtThoiGianVaoBanRutGon, txtTongSoMonAn,  txtTongTien;
       FlowLayoutPanel panelBanAn;
       Button btnGoiMon, btnThanhToan;
       DataGridView dataDanhSachMonAn;
       private TextBox txtBanSo;
       private Button btnChuyenBan;
       private ComboBox comboDanhSachBanTrong;

       public BanAnController(TextBox txtMaSo, TextBox txtThoiGianVaoBan,TextBox txtThoiGianVaoBanRutGon, TextBox txtTongSoMonAn,  TextBox txtTongTien, FlowLayoutPanel panelBanAn, Button btnGoiMon, Button btnThanhToan, DataGridView dataDanhSachMonAn)
       {
           // TODO: Complete member initialization
           this.txtMaSo = txtMaSo;
           this.txtThoiGianVaoBan = txtThoiGianVaoBan;
           this.txtThoiGianVaoBanRutGon = txtThoiGianVaoBanRutGon;
           this.txtTongSoMonAn = txtTongSoMonAn;

           this.txtTongTien = txtTongTien;
           this.panelBanAn = panelBanAn;
           this.btnGoiMon = btnGoiMon;
           this.btnThanhToan = btnThanhToan;
           this.dataDanhSachMonAn = dataDanhSachMonAn;
 
       }

       public BanAnController(TextBox txtBanSo, TextBox txtThoiGianVaoBan, TextBox txtThoiGianVaoBanRutGon, TextBox txtTongSoMonAn, TextBox txtTongTien, FlowLayoutPanel panelBanAn, Button btnGoiMon, Button btnThanhToan, Button btnChuyenBan, ComboBox comboDanhSachBanTrong, DataGridView dataDanhSachMonAn)
       {
           // TODO: Complete member initialization
           this.txtMaSo = txtBanSo;
           this.txtThoiGianVaoBan = txtThoiGianVaoBan;
           this.txtThoiGianVaoBanRutGon = txtThoiGianVaoBanRutGon;
           this.txtTongSoMonAn = txtTongSoMonAn;
           this.txtTongTien = txtTongTien;
           this.panelBanAn = panelBanAn;
           this.btnGoiMon = btnGoiMon;
           this.btnThanhToan = btnThanhToan;
           this.btnChuyenBan = btnChuyenBan;
           this.comboDanhSachBanTrong = comboDanhSachBanTrong;
           this.dataDanhSachMonAn = dataDanhSachMonAn;
       }

       public BanAnController(FlowLayoutPanel panelQuanLyBanAn, Button btnAnHien)
       {
           // TODO: Complete member initialization
           this.panelQuanLyBanAn = panelQuanLyBanAn;
           this.btnAnHien = btnAnHien;
           loadQuanLyBanAn();
       }



       




       //public BanAnController(TextBox txtThoiGianVaoBan, TextBox txtTongSoMonAn, TextBox txtKhuyenMai, TextBox txtTongTien, FlowLayoutPanel panel, Button btnGoiMon, Button btnThanhToan, DataGridView dataDanhSachMonAn)
       //{

       //}




       public void loadQuanLyBanAn()
       {
       

           panelQuanLyBanAn.Controls.Clear();
           List<BanAn> lstBanAn = new List<BanAn>();
           lstBanAn = sqlBanAn.loadListBanAnFull();

           foreach (BanAn i in lstBanAn)
           {
               Button b = new Button();
               b.Tag = i;
               b.Text = i.MaSo;
               b.Font = new Font("Arial", 12);
              
                   if (i.AnHien)
                   {
                       b.BackColor = Color.YellowGreen;
                   }
                   else
                   {

                       b.BackColor = Color.Gray;


                   }

                   if (!i.BanTrong)
                   {
                       b.Text += "\n(Có người)";
                      
                   }
               
      
               b.Width = 85;
               b.Height = 85;
               b.FlatStyle = FlatStyle.Flat;
               b.Click += b_Click;
        

               panelQuanLyBanAn.Controls.Add(b);
           }

       }

       BanAn banAn_QuanLyBanAn = new BanAn();
       //void b_Leave(object sender, EventArgs e)
       //{
       //    if (((BanAn)((Button)sender).Tag).AnHien)
       //    {
       //      (  (Button)sender).BackColor = Color.YellowGreen;
       //    }
       //    else
       //    {

       //        ((Button)sender).BackColor = Color.Gray;


       //    }


       //    btnAnHien.BackColor = Color.Empty;
           
       //}

       private void b_Click(object sender, EventArgs e)
       {
           foreach (Button i in panelQuanLyBanAn.Controls)
           {

               if (i.Focused)
               {
                   i.BackColor = Color.Pink;
               }
               else
               {
                   if (((BanAn)i.Tag).AnHien)
                   {
                       i.BackColor = Color.YellowGreen;
                   }else
                   {
                       i.BackColor = Color.Gray;
                   }
               }
           }
           banAn_QuanLyBanAn = ((BanAn)((Button)sender).Tag);
           xemAnHien((Button)sender);
           
       }

       public void xemAnHien(Button b)
       {

           if (((BanAn)(b).Tag).AnHien)
           {
               btnAnHien.BackColor = Color.Tomato;
               btnAnHien.Text = "Ẩn bàn ăn";
           }
           else
           {

              btnAnHien.BackColor = Color.Gray;
              btnAnHien.Text = "Hiện bàn ăn";

           }

          
           btnAnHien.Enabled = true;

       }

       public void anHienBanAn()
       {
           if (banAn_QuanLyBanAn.BanTrong)
           {

               if (banAn_QuanLyBanAn.AnHien)
               {
                   sqlBanAn.anBanAn(banAn_QuanLyBanAn.MaSo);
                   MessageBox.Show("Đã ẩn bàn ăn: " + banAn_QuanLyBanAn.MaSo, "");
               }
               else
               {
                   sqlBanAn.hienBanAn(banAn_QuanLyBanAn.MaSo);
                   MessageBox.Show("Đã hiện bàn ăn: " + banAn_QuanLyBanAn.MaSo, "");
               }
           }
           else
           {
               MessageBox.Show("Bàn có người, không thể ẩn được!", "Bàn có người rồi!");
           }
           
           loadQuanLyBanAn();
       }

       public void themBanAn()
       {
           MessageBox.Show("Đã thêm bàn ăn: " + sqlBanAn.getNewMaSo(), "Thêm bàn ăn thành công!");
           sqlBanAn.themBanAn();
           loadQuanLyBanAn();
           
       }
       
       





        //BAN AN
        public void loadBanAn()
        {

            panelBanAn.Controls.Clear();
            List<BanAn> lstBanAn = new List<BanAn>();
              lstBanAn =  sqlBanAn.loadListBanAn();

            foreach (BanAn i in lstBanAn)
            {
                Button a = new Button();
                a.Tag = i;
                a.Text = i.MaSo;
                a.Font = new Font("Arial", 12);
                if (i.BanTrong)
                {
                    a.BackColor = Color.YellowGreen;
                }
                else
                {
                    if (sqlBanAn.tongSoMonAn(i.MaSo) == 0)
                    {
                        a.BackColor = Color.SkyBlue;
                    }

                    else
                    {
                        a.BackColor = Color.Tomato;
                    }


                }
                a.Width = 85;
                a.Height = 85;
                a.FlatStyle = FlatStyle.Flat;


               


                a.Click += a_Click;
                a.MouseHover += a_MouseHover;
                panelBanAn.Controls.Add(a);
            }
            focusBan();
            toMauBan();


            comboDanhSachBanTrong.Items.Clear();
            List<BanAn> lstBanAnTrong = new List<BanAn>();
            lstBanAnTrong = sqlBanAn.loadListBanTrong();
            foreach (BanAn banAnTrong in lstBanAnTrong)
            {
                comboDanhSachBanTrong.Items.Add(banAnTrong.MaSo);

            }

            if (txtMaSo.Text.Equals(""))
            {
                btnChuyenBan.Enabled = false;
            }

        }


   
 

        void a_MouseHover(object sender, EventArgs e)
        {
            bool banTrong = ((BanAn)(((Button)sender).Tag)).BanTrong;

            if (banTrong)
            {

               (new ToolTip()).SetToolTip((Button)sender, "BÀN TRỐNG");
            }
            else
            {

                (new ToolTip()).SetToolTip((Button)sender, "ĐÃ CÓ NGƯỜI");
            }
        }

        void a_Click(object sender, EventArgs e)
        {
            bool banTrong = ((BanAn)(((Button)sender).Tag)).BanTrong;
            String maSo = ((BanAn)(((Button)sender).Tag)).MaSo;
            if (banTrong)
            {
                btnThanhToan.Enabled = false;
                btnChuyenBan.Enabled = false;
           
            }
            else
            {

                btnThanhToan.Enabled = true ;
                btnChuyenBan.Enabled = true;
             

            }
                hienThongTinBan(maSo);
                toMauBan(((Button)sender).Text);
                loadBanAn();
              
               

             
            
      
        }

        public void focusBan() {
            foreach (Button i in panelBanAn.Controls)
            {
                if (i.Text.Equals(txtMaSo.Text))
                {
                   
                    i.Focus();
                    
                }

            }
        }

        public void focusBan(string maSo)
        {
            foreach (Button i in panelBanAn.Controls)
            {
                if (i.Text.Equals(maSo))
                {

                    i.Focus();

                }

            }
        }

        public void toMauBan() {
            long tongTien = 0;
            try { tongTien = Convert.ToInt64(txtTongTien.Text.Replace(",","")); }
            catch { }


            foreach (Button i in panelBanAn.Controls)
            {

                if (i.Focused)
                {
                    if (((BanAn)(i.Tag)).BanTrong)
                    {
                        i.BackColor = Color.Pink;
                        txtMaSo.BackColor = i.BackColor;
                        txtTongTien.Text = "Bàn trống";
                        btnThanhToan.BackColor = Color.LightGray;
                        btnThanhToan.Text = "Bàn trống";
                        btnThanhToan.Enabled = false;

                    

                    }
                    else
                    {

                        
                            if (tongTien == 0)
                            {
                                hienThongTinBan(i.Text);
                                i.BackColor = Color.Thistle;
                                txtMaSo.BackColor = i.BackColor;
                                btnThanhToan.BackColor = Color.SkyBlue;
                                btnThanhToan.Text = "Trả bàn";
                                btnThanhToan.Enabled = true;
                             
                            }
                            else
                            {
                                hienThongTinBan(i.Text);
                                i.BackColor = Color.Gold;
                                txtMaSo.BackColor = i.BackColor;
                                btnThanhToan.BackColor = Color.YellowGreen;
                                btnThanhToan.Text = "Thanh toán";
                                btnThanhToan.Enabled = true;
                           
                            }
                        
                    }
               
                }
                
                
             

            }
        }




        public void toMauBan(string maSo)
        {
            long tongTien = 0;
            try { tongTien = Convert.ToInt64(txtTongTien.Text.Replace(",", "")); }
            catch { }


            foreach (Button i in panelBanAn.Controls)
            {

                if (i.Text.Equals(maSo))
                {
                    if (((BanAn)(i.Tag)).BanTrong)
                    {
                        i.BackColor = Color.Pink;
                        txtMaSo.BackColor = i.BackColor;
                        txtTongTien.Text = "Bàn trống";
                        btnThanhToan.BackColor = Color.LightGray;
                        btnThanhToan.Text = "Bàn trống";
                        btnThanhToan.Enabled = false;
                     

                    }
                    else
                    {


                        if (tongTien == 0)
                        {
                            hienThongTinBan(i.Text);
                            i.BackColor = Color.Thistle;
                            txtMaSo.BackColor = i.BackColor;
                            btnThanhToan.BackColor = Color.SkyBlue;
                            btnThanhToan.Text = "Trả bàn";
                            btnThanhToan.Enabled = true;
                       
                        }
                        else
                        {
                            hienThongTinBan(i.Text);
                            i.BackColor = Color.Gold;
                            txtMaSo.BackColor = i.BackColor;
                            btnThanhToan.BackColor = Color.YellowGreen;
                            btnThanhToan.Text = "Thanh toán";
                            btnThanhToan.Enabled = true;
                        
                        }

                    }

                }




            }
        }



        //int time = 60;
        //public void enableBtnThanhToan()
        //{
        //    Timer timer = new Timer();
        //    timer.Interval = 1000;
        //    timer.Tick +=timer_Tick;
        //    timer.Start();
        //}

        //void timer_Tick(object sender, EventArgs e)
        //{
        //   time--;
        //   if (time == 0)
        //   {
        //       ((Timer)sender).Stop();
        //       btnThanhToan.Enabled = true;
        //   }
        //}

      


        public void ngoiVaoBan(String maSo)
        {
           if(sqlBanAn.isBanTrong(maSo))
            
            sqlBanAn.ngoiVaoBan(maSo);


            hienThongTinBan(maSo);
            loadBanAn();

        }

        public void traBan(String maSo,DateTime thoiGianVaoBan, string tenTaiKhoan)
        {
            long tongTien = Convert.ToInt64(txtTongTien.Text.Replace(",", ""));
            if (tongTien == 0)
            {
                sqlBanAn.traBan(maSo, thoiGianVaoBan, tenTaiKhoan);
            }
            else
            {
                sqlBanAn.traBanThemHoaDon(maSo, thoiGianVaoBan, tenTaiKhoan);
            }
            resetTextBox();
            loadBanAn();

        }

        DateTime thoiGianVaoBanHienTai;
        private DataGridView dataBanAn;
        private DataGridView dataQuanLyBanAn;
        private FlowLayoutPanel panelQuanLyBanAn;
        private Button btnAnHien;


        public void chuyenBan()
        {

          string  maBanAn1 = txtMaSo.Text;
          string maBanAn2 = comboDanhSachBanTrong.GetItemText(comboDanhSachBanTrong.SelectedItem);
          DateTime  thoiGianVaoBan = thoiGianVaoBanHienTai;

          if (maBanAn1.Equals("") || maBanAn2.Equals(""))
          {
              if (maBanAn1.Equals(""))
              {
                  MessageBox.Show("Chưa chọn bàn cần chuyển!", "Chưa chọn bàn", MessageBoxButtons.OK, MessageBoxIcon.Stop);
  
                  return;
              }
              else if (maBanAn2.Equals(""))
              {
                  MessageBox.Show("Chưa chọn bàn sẽ chuyển đến!", "Chưa chọn bàn", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                  return;
              }
              
          }
          else
          {
              sqlBanAn.chuyenBan(maBanAn1, thoiGianVaoBan, maBanAn2);
              MessageBox.Show("Chuyển bàn thành công:\n" + maBanAn1 + " -> " + maBanAn2, "Chuyển bàn");
              resetTextBox();
              focusBan(maBanAn2);
              hienThongTinBan(maBanAn2);
              loadBanAn();
          }

          
            


        }

        public void hienThongTinBan(String maSo)
        {
            //  TextBox txtMaSo, txtGioVao, txtTongSoMonAn, txtKhuyenMai, txtTongTien;
            txtMaSo.Text = maSo;

            string thoiGian = DateTime.Now.ToString();
                DateTime thoiGianVaoBan = (DateTime.Now);
                int tongSoMonAn = 0;
                long khuyenMai = 0;
                long tongTien = 0;

                if (sqlBanAn.hienThiThongTinBanAn(ref maSo,ref  thoiGianVaoBan,ref tongSoMonAn,ref khuyenMai,ref tongTien))
                {


                    txtTongSoMonAn.Text = tongSoMonAn.ToString();
                    txtThoiGianVaoBan.Text = thoiGianVaoBan.ToString();
                    txtThoiGianVaoBanRutGon.Text = thoiGianVaoBan.ToString("HH:mm dd/MM");
                    this.thoiGianVaoBanHienTai = thoiGianVaoBan;

                    txtTongTien.Text = tongTien.ToString();

                    dataDanhSachMonAn.DataSource = (new GoiMonController()).loadDataDanhSachMonAn(txtMaSo.Text, thoiGianVaoBan.ToString());

                    
                    try
                    {
                        dataDanhSachMonAn.Columns["MASO"].Visible = false;
                        
                        dataDanhSachMonAn.Columns["TENMONAN"].HeaderText = "Tên món ăn";
                        dataDanhSachMonAn.Columns["GIATIEN"].HeaderText = "Giá tiền";
                        dataDanhSachMonAn.Columns["SOLUONG"].HeaderText = "Số lượng";
                    }
                    
                       


                    catch { }

                    

                }
                else
                {
                    resetTextBox();
                }
            

        }


        public void resetTextBox()
        {
            txtTongSoMonAn.Text = "";
            txtThoiGianVaoBan.Text = "";
            txtThoiGianVaoBanRutGon.Text = "";
            txtTongTien.Text = "";
            dataDanhSachMonAn.DataSource = null;
        }

        

    }
}
