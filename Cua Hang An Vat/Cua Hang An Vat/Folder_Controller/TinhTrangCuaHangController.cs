﻿using Cua_Hang_An_Vat.Folder_Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.Folder_Controller
{
    class TinhTrangCuaHangController
    {


        private System.Windows.Forms.ComboBox comboNgay;
        private System.Windows.Forms.ComboBox comboThang;
        private System.Windows.Forms.ComboBox comboNam;
        private System.Windows.Forms.TextBox txtTongDoanhThu;
        private System.Windows.Forms.TextBox txtMonAnGoiNhieuNhat;
        private System.Windows.Forms.TextBox txtBanAnCoDoanhThuCaoNhat;
        private System.Windows.Forms.TextBox txtBanAnNgoiNhieuNhat;
        private System.Windows.Forms.DataGridView dataMonAnGoiNhieuNhat;
        private System.Windows.Forms.DataGridView dataBanAnCoDoanhThuCaoNhat;
        private System.Windows.Forms.DataGridView dataBanAnNgoiNhieuNhat;

        //public TinhTrangCuaHangController(System.Windows.Forms.ComboBox comboNgay, System.Windows.Forms.ComboBox comboThang, System.Windows.Forms.ComboBox comboNam, System.Windows.Forms.TextBox txtTongDoanhThu, System.Windows.Forms.TextBox txtMonAnGoiNhieuNhat, System.Windows.Forms.TextBox txtBanAnCoDoanhThuCaoNhat, System.Windows.Forms.TextBox txtBanAnNgoiNhieuNhat, System.Windows.Forms.DataGridView dataMonAnGoiNhieuNhat, System.Windows.Forms.DataGridView dataBanAnCoDoanhThuCaoNhat, System.Windows.Forms.DataGridView dataBanAnNgoiNhieuNhat)
        //{
        //    // TODO: Complete member initialization
        //    this.comboNgay = comboNgay;
        //    this.comboThang = comboThang;
        //    this.comboNam = comboNam;
        //    this.txtTongDoanhThu = txtTongDoanhThu;
        //    this.txtMonAnGoiNhieuNhat = txtMonAnGoiNhieuNhat;
        //    this.txtBanAnCoDoanhThuCaoNhat = txtBanAnCoDoanhThuCaoNhat;
        //    this.txtBanAnNgoiNhieuNhat = txtBanAnNgoiNhieuNhat;
        //    this.dataMonAnGoiNhieuNhat = dataMonAnGoiNhieuNhat;
        //    this.dataBanAnCoDoanhThuCaoNhat = dataBanAnCoDoanhThuCaoNhat;
        //    this.dataBanAnNgoiNhieuNhat = dataBanAnNgoiNhieuNhat;

           


        //    //   comboBox1.SelectedIndex = comboBox1.Items.IndexOf("test1");


        //}

        public TinhTrangCuaHangController(ComboBox comboNgay, ComboBox comboThang, ComboBox comboNam, TextBox txtTongDoanhThu, DataGridView dataMonAnGoiNhieuNhat, DataGridView dataBanAnCoDoanhThuCaoNhat, DataGridView dataBanAnNgoiNhieuNhat)
        {
            // TODO: Complete member initialization
            this.comboNgay = comboNgay;
            this.comboThang = comboThang;
            this.comboNam = comboNam;
            this.txtTongDoanhThu = txtTongDoanhThu;
            this.dataMonAnGoiNhieuNhat = dataMonAnGoiNhieuNhat;
            this.dataBanAnCoDoanhThuCaoNhat = dataBanAnCoDoanhThuCaoNhat;
            this.dataBanAnNgoiNhieuNhat = dataBanAnNgoiNhieuNhat;
            khoiTao();
        }

        int ngay, thang;
        int nam = DateTime.Now.Year;

        public void khoiTao()
        {

            List<int> lstNam = (new SQLTinhTrangCuaHang()).loadNam();
            foreach (int i in lstNam)
            {
                comboNam.Items.Add(i);
            }
            

            comboThang.Items.Add("--XEM THEO NĂM--");
            for (int i = 1; i <= 12; i++)
            {
                comboThang.Items.Add(i);
            }
            comboNgay.Enabled = false;

            xemTinhTrang();

        }

        public void chonNam()
        {
            string selected = this.comboNam.GetItemText(this.comboNam.SelectedItem);
            this.nam = Convert.ToInt32(selected);
            ngay = 0;
            thang = 0;
            
            xemTinhTrang();
        }
        public void chonThang()
        {
            string selected = this.comboThang.GetItemText(this.comboThang.SelectedItem);
            try
            {
                this.thang = Convert.ToInt32(selected);
                comboNgay.Enabled = true;
                comboNgay.Items.Clear();
                comboNgay.ResetText();
                comboNgay.Items.Add("--XEM THEO THÁNG--");
                if (thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12)
                {
                    for (int i = 1; i <= 31; i++)
                    {
                        comboNgay.Items.Add(i);
                    }
                }
                else if (thang == 2 && DateTime.IsLeapYear(nam))
                {
                    for (int i = 1; i <= 29; i++)
                    {
                        comboNgay.Items.Add(i);
                    }
                }
                else if (thang == 2 && !DateTime.IsLeapYear(nam))
                {
                    for (int i = 1; i <= 28; i++)
                    {
                        comboNgay.Items.Add(i);
                    }
                }
                else
                {
                    for (int i = 1; i <= 30; i++)
                    {
                        comboNgay.Items.Add(i);
                    }
                }

                ngay = 0;
            }
            catch (Exception e)
            {
                ngay = 0;
                thang = 0;
                comboNgay.Enabled = false;
            }

            xemTinhTrang();

        }
        public void chonNgay()
        {
             string selected = this.comboNgay.GetItemText(this.comboNgay.SelectedItem);
             try
             {
                 this.ngay = Convert.ToInt32(selected);
             }
             catch {
                 ngay = 0;
                 
             }

             xemTinhTrang();
        }

        SQLTinhTrangCuaHang sqlTinhTrangCuaHang = new SQLTinhTrangCuaHang();
        public void xemTinhTrang()
        {
            if (ngay != 0 && thang != 0)
            {
                txtTongDoanhThu.Text = "" + sqlTinhTrangCuaHang.tongDoanhThuTheoNgay(ngay, thang, nam);



                dataMonAnGoiNhieuNhat.DataSource = sqlTinhTrangCuaHang.monAnGoiNhieuNhatTheoNgay(ngay, thang, nam);
                dataBanAnCoDoanhThuCaoNhat.DataSource = sqlTinhTrangCuaHang.banAnCoDoanhThuNhieuNhatNgay(ngay, thang, nam);
                dataBanAnNgoiNhieuNhat.DataSource = sqlTinhTrangCuaHang.banAnNgoiNhieuNhatNgay(ngay, thang, nam);
            }
            else if (ngay == 0 && thang != 0)
            {
                comboNgay.SelectedIndex = -1;
                txtTongDoanhThu.Text = "" + sqlTinhTrangCuaHang.tongDoanhThuTheoThang(thang, nam);



                dataMonAnGoiNhieuNhat.DataSource = sqlTinhTrangCuaHang.monAnGoiNhieuNhatTheoThang(thang, nam);
                dataBanAnCoDoanhThuCaoNhat.DataSource = sqlTinhTrangCuaHang.banAnCoDoanhThuNhieuNhatThang(thang, nam);
                dataBanAnNgoiNhieuNhat.DataSource = sqlTinhTrangCuaHang.banAnNgoiNhieuNhatThang(thang, nam);
            }
            else if (ngay == 0 && thang == 0)
            {
                comboNgay.SelectedIndex = -1;
                comboThang.SelectedIndex = -1;

                txtTongDoanhThu.Text = "" + sqlTinhTrangCuaHang.tongDoanhThuTheoNam(nam);



                dataMonAnGoiNhieuNhat.DataSource = sqlTinhTrangCuaHang.monAnGoiNhieuNhatTheoNam(nam);
                dataBanAnCoDoanhThuCaoNhat.DataSource = sqlTinhTrangCuaHang.banAnCoDoanhThuNhieuNhatNam(nam);
                dataBanAnNgoiNhieuNhat.DataSource = sqlTinhTrangCuaHang.banAnNgoiNhieuNhatNam(nam);

            }
        }
    }
}
