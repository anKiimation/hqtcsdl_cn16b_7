﻿using Cua_Hang_An_Vat.DTO;
using Cua_Hang_An_Vat.Folder_Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.Folder_Controller
{
    class GoiMonController
    {
        SQLGoiMon sqlGoiMon = new SQLGoiMon();
        Button btnThemMonAn, btnXoaMonAn,btnGoiMon;
        DataGridView dataMonAn, dataMonGoi;
        TextBox txtMaBanAn, txtThoiGianVaoBan;
        //private TextBox txtMaSo;
        //private TextBox txtTenMonAn;
        //private TextBox txtNhomMonAn;
        //private TextBox txtGiaTien;
        private TextBox txtTimKiem;
        List<MonAn.MonAnDaGoi> lstMonAnDaGoi = new List<MonAn.MonAnDaGoi>();
        private TextBox txtSoLuongThem;
        private TextBox txtSoLuongGiam;
        private TextBox txtTenMonAn;
        public void refresh()
        {
            dataMonAn.DataSource = (new SQLMonAn()).loadDataMonAnKhaDung();
            dataMonAn.Columns["MASO"].Visible = false;
            dataMonAn.Columns["KHADUNG"].Visible = false;
            dataMonAn.Columns["TENMONAN"].HeaderText = "Tên món ăn";
            dataMonAn.Columns["GIATIEN"].HeaderText = "Giá tiền";


            dataMonAn.ClearSelection();
            dataMonGoi.ClearSelection();
            

        }




        //public GoiMonController(TextBox txtMaBanAn, TextBox txtThoiGianVaoBan, DataGridView dataMonAn, DataGridView dataMonGoi, Button btnThemMonAn, Button btnXoaMonAn, Button btnGoiMon)
        //{
        //    // TODO: Complete member initialization
        //    this.txtMaBanAn = txtMaBanAn;
        //    this.txtThoiGianVaoBan = txtThoiGianVaoBan;
        //    this.dataMonAn = dataMonAn;
        //    this.dataMonGoi = dataMonGoi;
        //    this.btnThemMonAn = btnThemMonAn;
        //    this.btnXoaMonAn = btnXoaMonAn;
        //    this.btnGoiMon = btnGoiMon;

        //    refresh();
        //}

        //public GoiMonController(TextBox txtMaBanAn, TextBox txtThoiGianVaoBan, TextBox txtMaSo, TextBox txtTenMonAn, TextBox txtNhomMonAn, TextBox txtGiaTien, DataGridView dataMonAn, DataGridView dataMonGoi, Button btnThemMonAn, Button btnXoaMonAn, Button btnGoiMon)
        //{
        //    // TODO: Complete member initialization
        //    this.txtMaBanAn = txtMaBanAn;
        //    this.txtThoiGianVaoBan = txtThoiGianVaoBan;
        //    //this.txtMaSo = txtMaSo;
        //    //this.txtTenMonAn = txtTenMonAn;
        //    //this.txtNhomMonAn = txtNhomMonAn;
        //    //this.txtGiaTien = txtGiaTien;
        //    this.dataMonAn = dataMonAn;
        //    this.dataMonGoi = dataMonGoi;
        //    this.btnThemMonAn = btnThemMonAn;
        //    this.btnXoaMonAn = btnXoaMonAn;
        //    this.btnGoiMon = btnGoiMon;

        //    refresh();
        //}

        public GoiMonController()
        {
            // TODO: Complete member initialization
        }

        public GoiMonController(TextBox txtTimKiem, TextBox txtMaBanAn, TextBox txtThoiGianVaoBan, TextBox txtTenMonAn, TextBox txtSoLuongThem, TextBox txtSoLuongGiam, DataGridView dataMonAn, DataGridView dataMonGoi, Button btnThemMonAn, Button btnXoaMonAn, Button btnGoiMon)
        {
            // TODO: Complete member initialization
            this.txtTimKiem = txtTimKiem;
            this.txtMaBanAn = txtMaBanAn;
            this.txtThoiGianVaoBan = txtThoiGianVaoBan;
            this.txtTenMonAn = txtTenMonAn;
            this.txtSoLuongThem = txtSoLuongThem;
            this.txtSoLuongGiam = txtSoLuongGiam;
            this.dataMonAn = dataMonAn;
            this.dataMonGoi = dataMonGoi;
            this.btnThemMonAn = btnThemMonAn;
            this.btnXoaMonAn = btnXoaMonAn;
            this.btnGoiMon = btnGoiMon;
            refresh();
        }

        //public GoiMonController(TextBox txtTimKiem, TextBox txtMaBanAn, TextBox txtThoiGianVaoBan, TextBox txtMaSo, TextBox txtTenMonAn, TextBox txtNhomMonAn, TextBox txtGiaTien, DataGridView dataMonAn, DataGridView dataMonGoi, Button btnThemMonAn, Button btnXoaMonAn, Button btnGoiMon)
        //{
        //    // TODO: Complete member initialization
        //    this.txtTimKiem = txtTimKiem;
        //    this.txtMaBanAn = txtMaBanAn;
        //    this.txtThoiGianVaoBan = txtThoiGianVaoBan;
        //    //this.txtMaSo = txtMaSo;
        //    //this.txtTenMonAn = txtTenMonAn;
        //    //this.txtNhomMonAn = txtNhomMonAn;
        //    //this.txtGiaTien = txtGiaTien;
        //    this.dataMonAn = dataMonAn;
        //    this.dataMonGoi = dataMonGoi;
        //    this.btnThemMonAn = btnThemMonAn;
        //    this.btnXoaMonAn = btnXoaMonAn;
        //    this.btnGoiMon = btnGoiMon;

        //    refresh();
        //}

        //public GoiMonController(TextBox txtTimKiem, TextBox txtMaBanAn, TextBox txtThoiGianVaoBan, TextBox txtSoLuongThem, TextBox txtSoLuongGiam, DataGridView dataMonAn, DataGridView dataMonGoi, Button btnThemMonAn, Button btnXoaMonAn, Button btnGoiMon)
        //{
        //    // TODO: Complete member initialization
        //    this.txtTimKiem = txtTimKiem;
        //    this.txtMaBanAn = txtMaBanAn;
        //    this.txtThoiGianVaoBan = txtThoiGianVaoBan;
        //    this.txtSoLuongThem = txtSoLuongThem;
        //    this.txtSoLuongGiam = txtSoLuongGiam;
        //    this.dataMonAn = dataMonAn;
        //    this.dataMonGoi = dataMonGoi;
        //    this.btnThemMonAn = btnThemMonAn;
        //    this.btnXoaMonAn = btnXoaMonAn;
        //    this.btnGoiMon = btnGoiMon;

        //    refresh();
        //}


        //public GoiMonController(TextBox txtTimKiem, TextBox txtMaBanAn, TextBox txtThoiGianVaoBan, TextBox tenMonAn, TextBox txtSoLuongThem, TextBox txtSoLuongGiam, DataGridView dataMonAn, DataGridView dataMonGoi, Button btnThemMonAn, Button btnXoaMonAn, Button btnGoiMon)
        //{
        //    // TODO: Complete member initialization
        //    this.txtTimKiem = txtTimKiem;
        //    this.txtMaBanAn = txtMaBanAn;
        //    this.txtThoiGianVaoBan = txtThoiGianVaoBan;
        //    this.txtSoLuongThem = txtSoLuongThem;
        //    this.txtSoLuongGiam = txtSoLuongGiam;
        //    this.dataMonAn = dataMonAn;
        //    this.dataMonGoi = dataMonGoi;
        //    this.btnThemMonAn = btnThemMonAn;
        //    this.btnXoaMonAn = btnXoaMonAn;
        //    this.btnGoiMon = btnGoiMon;

        //    refresh();
        //}


        private MonAn monAnDaChon;
        private MonAn.MonAnDaGoi monAnDaGoi;
        
        public void hienThiThongTinMonAnDaChon(int index)
        {
            if (index >= 0)
            {
                MonAn monAnDaChonTemp = new MonAn();

                dataMonAn.Rows[index].Selected = true;

                DataGridViewRow row = this.dataMonAn.Rows[index];
                monAnDaChonTemp.MaSo = row.Cells[0].Value.ToString();
                monAnDaChonTemp.TenMonAn = row.Cells[1].Value.ToString();
                //txtNhomMonAn.Text = row.Cells[2].Value.ToString();
                monAnDaChonTemp.GiaTien = Convert.ToInt64(row.Cells[2].Value.ToString());

                monAnDaChon = monAnDaChonTemp;
                txtTenMonAn.Text = monAnDaChon.TenMonAn;
               
            }
        }

        public void hienThiThongTinMonAnDaGoi(int index)
        {
            if (index >= 0)
            {
                MonAn.MonAnDaGoi monAnDaGoiTemp = new MonAn.MonAnDaGoi();

                dataMonGoi.Rows[index].Selected = true;

                DataGridViewRow row = this.dataMonGoi.Rows[index];
                monAnDaGoiTemp.MaSo = row.Cells[0].Value.ToString();
                monAnDaGoiTemp.TenMonAn = row.Cells[1].Value.ToString();
                //txtGiaTien.Text = (Convert.ToInt64(row.Cells[2].Value.ToString()) * Convert.ToInt32(row.Cells[3].Value.ToString())).ToString();
                //txtNhomMonAn.Text = "";
                //txtTenMonAn.Text = row.Cells[1].Value.ToString();
                //txtNhomMonAn.Text = row.Cells[2].Value.ToString();
                //txtGiaTien.Text = row.Cells[3].Value.ToString();

                monAnDaGoi = monAnDaGoiTemp;
                txtTenMonAn.Text = monAnDaGoi.TenMonAn;
            }
        }

        public bool checkItemInList(List<MonAn.MonAnDaGoi> lst, string maSo)
        {
            foreach (MonAn.MonAnDaGoi i in lstMonAnDaGoi)
            {
                if (i.MaSo.Equals(maSo))
                {
                    
                    return true;
                }
            }
            return false;
        }

        public void themMonAn()
        {
            try
            {
                int soLuong =Convert.ToInt32( txtSoLuongThem.Text);
                //MonAn.MonAnDaGoi a = new MonAn.MonAnDaGoi(txtMaSo.Text, txtTenMonAn.Text, txtNhomMonAn.Text, Convert.ToInt64(txtGiaTien.Text), 1);
                MonAn.MonAnDaGoi a = new MonAn.MonAnDaGoi(monAnDaChon.MaSo, monAnDaChon.TenMonAn,Convert.ToInt64(monAnDaChon.GiaTien), soLuong);
                if (checkItemInList(lstMonAnDaGoi, a.MaSo))
                    foreach (MonAn.MonAnDaGoi i in lstMonAnDaGoi)
                    {
                        if (i.MaSo.Equals(a.MaSo))
                        {
                            i.SoLuong += soLuong;

                        }
                    }
                else
                    lstMonAnDaGoi.Add(a);
                dataMonGoi.Rows.Clear();

                foreach (MonAn.MonAnDaGoi i in lstMonAnDaGoi)
                {
                    dataMonGoi.Rows.Add(i.MaSo, i.TenMonAn, i.GiaTien, i.SoLuong);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Vui lòng chọn món!", "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            resetMonAn();
            refresh();
               
        }

        public void xoaMonAn()
        {
            try
            {
                int soLuong = Convert.ToInt32(txtSoLuongGiam.Text);
                string maMonAn = monAnDaGoi.MaSo;
                foreach (MonAn.MonAnDaGoi i in lstMonAnDaGoi)
                {
                    if (i.MaSo.Equals(maMonAn))
                    {
                        if (i.SoLuong > 0 && soLuong <= i.SoLuong && soLuong >= 0)
                        {
                            i.SoLuong -= soLuong;
                        }
                        else { MessageBox.Show("Số lượng sai", "Lỗi"); }


                    }
                }

                foreach (MonAn.MonAnDaGoi i in lstMonAnDaGoi)
                {
                    if (i.SoLuong == 0)
                    {
                        lstMonAnDaGoi.Remove(i);
                        break;
                    }
                }

                dataMonGoi.Rows.Clear();
                foreach (MonAn.MonAnDaGoi i in lstMonAnDaGoi)
                {
                    dataMonGoi.Rows.Add(i.MaSo, i.TenMonAn, i.GiaTien, i.SoLuong);
                }
            }
            catch
            {
                MessageBox.Show("Vui lòng chọn món!", "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            resetMonAn();
            refresh();
        }

        public void resetMonAn()
        {
            //this.txtMaSo.Text = "";
            //this.txtTenMonAn.Text = "";
            //this.txtNhomMonAn.Text = "";
            //this.txtGiaTien.Text = "";
            monAnDaChon = null;
            monAnDaGoi = null;
        }

        public void goiMon()
        {
            string maBanAn = txtMaBanAn.Text;
            string thoiGianVaoBan = txtThoiGianVaoBan.Text;
            if (lstMonAnDaGoi.Count > 0)
            {

                foreach (MonAn.MonAnDaGoi i in lstMonAnDaGoi)
                {
                    while (i.SoLuong > 0)
                    {
                        sqlGoiMon.themMonAn(maBanAn, Convert.ToDateTime(thoiGianVaoBan), i.MaSo);
                        i.SoLuong--;
                    }
                }
                MessageBox.Show("Gọi món thành công!", "Thành công!");
            }


            //foreach (MonAn.MonAnDaGoi i in sqlGoiMon.loadListGoiMon(maBanAn, Convert.ToDateTime(thoiGianVaoBan)))
            //{
            //    abc += i.TenMonAn + "- So luong: " + i.SoLuong + "\n";
            //}
            //MessageBox.Show(abc, "");

        }

        public void timKiemMonAn()
        {
            dataMonAn.DataSource = (new SQLMonAn()).timKiemMonAn(txtTimKiem.Text);

        }

        public DataTable loadDataDanhSachMonAn(string maBanAn, string thoiGianVaoBan){
            try
            {
                return sqlGoiMon.loadDataGoiMonChiTiet(maBanAn, Convert.ToDateTime(thoiGianVaoBan));
            }catch
            {
            
            }
            return null;
        }


    }
}
