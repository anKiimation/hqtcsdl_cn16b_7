﻿using Cua_Hang_An_Vat.Folder_Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.Folder_Controller
{
    class TaiKhoanController
    {
        SQLTaiKhoan sqlTaiKhoan = new SQLTaiKhoan();
        DataGridView dataTaiKhoan;
        TextBox txtTenTaiKhoan, txtMatKhau, txtTenNhanVien, txtSoDienThoai, txtDiaChi, txtTimKiem;
        DateTimePicker dateNgaySinh;
        private ComboBox comboChucVu;




        public TaiKhoanController(TextBox txtTenTaiKhoan, TextBox txtTenNhanVien, DateTimePicker dateNgaySinh, TextBox txtSoDienThoai, TextBox txtDiaChi)
        {
            // TODO: Complete member initialization
            this.txtTenTaiKhoan = txtTenTaiKhoan;
            this.txtTenNhanVien = txtTenNhanVien;
            this.dateNgaySinh = dateNgaySinh;
            this.txtSoDienThoai = txtSoDienThoai;
            this.txtDiaChi = txtDiaChi;

            Refresh();
           

        }

        public TaiKhoanController(TextBox txtTenTaiKhoan, TextBox txtMatKhau, TextBox txtTenNhanVien, DateTimePicker dateNgaySinh, TextBox txtSoDienThoai, TextBox txtDiaChi)
        {
            // TODO: Complete member initialization
            this.txtTenTaiKhoan = txtTenTaiKhoan;
            this.txtMatKhau = txtMatKhau;
            this.txtTenNhanVien = txtTenNhanVien;
            this.dateNgaySinh = dateNgaySinh;
            this.txtSoDienThoai = txtSoDienThoai;
            this.txtDiaChi = txtDiaChi;
           
        }



        public TaiKhoanController()
        {
            // TODO: Complete member initialization
        }


        public TaiKhoanController(DataGridView dataTaiKhoan, TextBox txtTenTaiKhoan, TextBox txtTenNhanVien, DateTimePicker dateNgaySinh, TextBox txtSoDienThoai, TextBox txtDiaChi, TextBox txtTimKiem, ComboBox comboChucVu)
        {
            // TODO: Complete member initialization
            this.dataTaiKhoan = dataTaiKhoan;
            this.txtTenTaiKhoan = txtTenTaiKhoan;
            this.txtTenNhanVien = txtTenNhanVien;
            this.dateNgaySinh = dateNgaySinh;
            this.txtSoDienThoai = txtSoDienThoai;
            this.txtDiaChi = txtDiaChi;
            this.txtTimKiem = txtTimKiem;
            this.comboChucVu = comboChucVu;
            Refresh();
        }
        public void Refresh()
        {
            dataTaiKhoan.DataSource = (new SQLTaiKhoan()).loadDataTaiKhoan();
            dataTaiKhoan.Columns["tentaikhoan"].HeaderText = "Tên tài khoản";
            dataTaiKhoan.Columns["tennhanvien"].HeaderText = "Họ tên";
            dataTaiKhoan.Columns["ngaysinh"].HeaderText = "Ngày sinh";
            dataTaiKhoan.Columns["sodienthoai"].HeaderText = "Điện thoại";
            dataTaiKhoan.Columns["diachi"].HeaderText = "Địa chỉ";
            dataTaiKhoan.Columns["isadmin"].HeaderText = "Admin?";
            dataTaiKhoan.Columns["MATKHAU"].Visible = false;

        }


        public void thongBao(bool kq)
        {
            if (kq)
                MessageBox.Show("Thực hiện thành công!", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                MessageBox.Show("Thực hiện không thành công!", "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void thongBao(Exception e)
        {

            MessageBox.Show("Thực hiện không thành công!\n" + "Lỗi: " + e.Message, "Thất bại", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }


        public string taoMatKhau(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }


        //chuc nang 
        public void themTaiKhoan()
        {
            string tenDangNhap = txtTenTaiKhoan.Text;
            if (tenDangNhap.Equals(""))
            {
                thongBao(false);
            }


            else {


            string matKhau = taoMatKhau(txtMatKhau.Text);
            string tenNhanVien = txtTenNhanVien.Text;
            string ngaySinh = dateNgaySinh.Value.ToShortDateString();
            string soDienThoai = txtSoDienThoai.Text;
            string diaChi = txtDiaChi.Text;

            sqlTaiKhoan.themTaiKhoan(tenDangNhap, matKhau, tenNhanVien, ngaySinh, soDienThoai, diaChi);

            thongBao(true);

            }
        }

        public void resetTextBox()
        {
            txtTenTaiKhoan.Text = "";
            txtTenNhanVien.Text = "";
            txtSoDienThoai.Text = "";
            dateNgaySinh.Text = "";
            txtDiaChi.Text = "";
        }

        public void hienThiThongTinKhiChonVaoDataGridView(int index)
        {
            if (index >= 0)
            {
                dataTaiKhoan.Rows[index].Selected = true;

                DataGridViewRow row = this.dataTaiKhoan.Rows[index];
                txtTenTaiKhoan.Text = row.Cells["TENTAIKHOAN"].Value.ToString();
                txtTenNhanVien.Text = row.Cells["TENNHANVIEN"].Value.ToString();
                dateNgaySinh.Text = row.Cells["NGAYSINH"].Value.ToString();
                txtSoDienThoai.Text = row.Cells["SODIENTHOAI"].Value.ToString();
                txtDiaChi.Text = row.Cells["DIACHI"].Value.ToString();

                bool isAdmin = Convert.ToBoolean(row.Cells["ISADMIN"].Value.ToString());
                if (isAdmin)
                {
                    comboChucVu.SelectedIndex = comboChucVu.FindString("Quản lý");
                }
                else
                {
                    comboChucVu.SelectedIndex = comboChucVu.FindString("Nhân viên");
                }



            }
        }


        public void xoaTaiKhoan()
        {
            string tenDangNhap = txtTenTaiKhoan.Text;
            sqlTaiKhoan.xoaTaiKhoan(tenDangNhap);
            thongBao(true);
            Refresh();
        }

        public void suaTaiKhoan()
        {
            string tenDangNhap = txtTenTaiKhoan.Text;

            string tenNhanVien = txtTenNhanVien.Text;
            string ngaySinh = dateNgaySinh.Value.ToShortDateString();
            string soDienThoai = txtSoDienThoai.Text;
            string diaChi = txtDiaChi.Text;

            string stringChucVu = comboChucVu.SelectedItem.ToString();
            bool isAdmin = true;
            if (stringChucVu.Equals("Quản lý"))
            {
                isAdmin = true;
            }
            else
            {
                isAdmin = false;
            }

            sqlTaiKhoan.suaTaiKhoan(tenDangNhap, tenNhanVien, ngaySinh, soDienThoai, diaChi, isAdmin);

                 thongBao(true);
                 Refresh();

                 resetTextBox();
        }

        public void doiMatKhau(string tenTaiKhoan, string matKhauMoi)
        {
            //if (taoMatKhau(matKhauCu).Equals()) { 

            sqlTaiKhoan.doiMatKhau(tenTaiKhoan, matKhauMoi);
            thongBao(true);
        }

        public void timKiemTaiKhoan()
        {
            dataTaiKhoan.DataSource =  (new SQLTaiKhoan()).timKiemTaiKhoan(txtTimKiem.Text);
            dataTaiKhoan.Columns["MATKHAU"].Visible = false;
        }

    }
}
