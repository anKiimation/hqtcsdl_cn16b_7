﻿using Cua_Hang_An_Vat.FolderForm;
using Cua_Hang_An_Vat.GUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new FormDangNhap());
        }

        public static void tienTe(TextBox txt)
        {
            try
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                decimal value = decimal.Parse(txt.Text, System.Globalization.NumberStyles.AllowThousands);
                txt.Text = String.Format(culture, "{0:N0}", value);
                txt.Select(txt.Text.Length, 0);
            }
            catch { }
        }

        public static void khoiTaoControl(Form form)
        {

            form.FormBorderStyle = FormBorderStyle.FixedSingle;
            form.ControlBox = false;
            foreach (Control i in form.Controls)
            {

                if (i.HasChildren) //neu no la container
                {

                    ((GroupBox)i).FlatStyle = FlatStyle.Flat;
                    foreach (Control j in i.Controls)
                    {
                        if (j is Button)
                        {
                            ((Button)j).FlatStyle = FlatStyle.Flat;

                        }
                        if (j is TextBox)
                        {
                            ((TextBox)j).BorderStyle = BorderStyle.FixedSingle;

                        }
                        if (j is DataGridView)
                        {
                            ((DataGridView)j).BorderStyle = BorderStyle.Fixed3D;
                            //DataGridViewCellStyle { BackColor=Color [A=255, R=224, G=224, B=224], SelectionBackColor=Color [A=255, R=0, G=64, B=0] }
                            ((DataGridView)j).AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;


                            ((DataGridView)j).AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                            ((DataGridView)j).BackgroundColor = Color.White;
                            ((DataGridView)j).RowHeadersVisible = false;
                        }
                    }
                }
                else //neu ko thi
                {
                    if (i is Button)
                    {
                        ((Button)i).FlatStyle = FlatStyle.Flat;

                    }
                    if (i is TextBox)
                    {
                        ((TextBox)i).BorderStyle = BorderStyle.FixedSingle;

                    }
                    if (i is DataGridView)
                    {
                        ((DataGridView)i).BorderStyle = BorderStyle.Fixed3D;
                        //DataGridViewCellStyle { BackColor=Color [A=255, R=224, G=224, B=224], SelectionBackColor=Color [A=255, R=0, G=64, B=0] }
                        ((DataGridView)i).AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;

                        ((DataGridView)i).AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                        ((DataGridView)i).BackgroundColor = Color.White;
                        ((DataGridView)i).RowHeadersVisible = false;
                    }
                }

            }


        }



    }
}
