﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.Folder_Class
{
    class GoiMon
    {
        private string maBanAn;

        public string MaBanAn
        {
            get { return maBanAn; }
            set { maBanAn = value; }
        }
        private DateTime thoiGianVaoBan;

        public DateTime ThoiGianVaoBan
        {
            get { return thoiGianVaoBan; }
            set { thoiGianVaoBan = value; }
        }
        private string maMonAn;

        public string MaMonAn
        {
            get { return maMonAn; }
            set { maMonAn = value; }
        }
    }
}
