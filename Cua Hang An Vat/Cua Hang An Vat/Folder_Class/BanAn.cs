﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.DTO
{
    class BanAn
    {
        private String maSo;
        private bool banTrong;
        private bool anHien;

       
        private DateTime gioVao;
        private int tongSoMonAn;
        private long tongTien;
        private List<MonAn> lstMonAn = new List<MonAn>();

        //set get
        public DateTime GioVao
        {
            get { return gioVao; }
            set { gioVao = value; }
        }
        public int TongSoMonAn
        {
            get { return tongSoMonAn; }
            set { tongSoMonAn = value; }
        }
        public long TongTien
        {
            get { return tongTien; }
            set { tongTien = value; }
        }
        public String MaSo
        {
            get { return maSo; }
            set { maSo = value; }
        }

       

        public bool BanTrong
        {
            get { return banTrong; }
            set { banTrong = value; }
        }
        public bool AnHien
        {
            get { return anHien; }
            set { anHien = value; }
        }

        public List<MonAn> LstMonAn
        {
            get { return lstMonAn; }
            set { lstMonAn = value; }
        }


        //ham
        public BanAn() { }

        public BanAn(string maSo, bool banTrong,bool anHien)
        {
            this.maSo = maSo;
            this.banTrong = banTrong;
            this.anHien = anHien;
        }
    }
}
