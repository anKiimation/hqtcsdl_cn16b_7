﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.Folder_Class
{
    class HoaDon
    {
        private string maBanAn;

        public string MaBanAn
        {
            get { return maBanAn; }
            set { maBanAn = value; }
        }
        private string thoiGianVaoBan;

        public string ThoiGianVaoBan
        {
            get { return thoiGianVaoBan; }
            set { thoiGianVaoBan = value; }
        }
        
        private int tongSoMonAn;

        public int TongSoMonAn
        {
            get { return tongSoMonAn; }
            set { tongSoMonAn = value; }
        }
        private string tenTaiKhoan;

        public string TenTaiKhoan
        {
            get { return tenTaiKhoan; }
            set { tenTaiKhoan = value; }
        }

        public HoaDon(string maBanAn, string thoiGianVaoBan, int tongSoMonAn, long tongTien, string tenTaiKhoan)
        {
            // TODO: Complete member initialization
            this.maBanAn = maBanAn;
            this.thoiGianVaoBan = thoiGianVaoBan;
            this.tongSoMonAn = tongSoMonAn;
            this.tongTien = tongTien;
            this.tenTaiKhoan = tenTaiKhoan;
        }

        private long tongTien;

        public long TongTien
        {
            get { return tongTien; }
            set { tongTien = value; }
        }
      
    }
}
