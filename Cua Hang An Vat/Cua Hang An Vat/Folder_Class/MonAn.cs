﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.DTO
{
   public class MonAn
    {
        private String maSo;
        private String tenMonAn;
        private String nhomMonAn;
        private long giaTien;
        private bool khaDung;


   

        //set get
        public bool KhaDung
        {
            get { return khaDung; }
            set { khaDung = value; }
        }


        public String MaSo
        {
            get { return maSo; }
            set { maSo = value; }
        }
       

        public String TenMonAn
        {
            get { return tenMonAn; }
            set { tenMonAn = value; }
        }
        


       

        public long GiaTien
        {
            get { return giaTien; }
            set { giaTien = value; }
        }

        public MonAn() { }

        public MonAn(string maSo, string tenMonAn, long giaTien, bool khaDung)
        {
            this.maSo = maSo;
            this.tenMonAn = tenMonAn;

            this.giaTien = giaTien;
            this.khaDung = khaDung;
        }

        public MonAn(string p1, string p2, long p4)
        {
            // TODO: Complete member initialization
            this.maSo = p1;
            this.tenMonAn = p2;

            this.giaTien = p4;
            this.khaDung = true;
        }

        public class MonAnDaGoi{
            private string maSo;

public string MaSo
{
  get { return maSo; }
  set { maSo = value; }
}
            private string tenMonAn;

public string TenMonAn
{
  get { return tenMonAn; }
  set { tenMonAn = value; }
}
            private string nhomMonAn;

public string NhomMonAn
{
  get { return nhomMonAn; }
  set { nhomMonAn = value; }
}
            private long giaTien;

public long GiaTien
{
  get { return giaTien; }
  set { giaTien = value; }
}
private int soLuong;


public int SoLuong
{
    get { return soLuong; }
    set { soLuong = value; }
}

            public MonAnDaGoi(string p1, string p2, string p3, long p4, int p5)
            {
                // TODO: Complete member initialization
                this.maSo = p1;
                this.tenMonAn = p2;
                this.nhomMonAn = p3;
                this.giaTien = p4;
                this.soLuong=p5;
            }

            public MonAnDaGoi(string maSo, string tenMonAn, string nhomMonAn, long giaTien)
            {
                // TODO: Complete member initialization
                this.maSo = maSo;
                this.tenMonAn = tenMonAn;
                this.nhomMonAn = nhomMonAn;
                this.giaTien = giaTien;
                this.soLuong = 1;
            }

            public MonAnDaGoi(string p1, string p2, long p3, int p4)
            {
                // TODO: Complete member initialization
                this.maSo = p1;
                this.tenMonAn = p2;
                this.giaTien = p3;
                this.soLuong = p4;
            }

            public MonAnDaGoi()
            {
                // TODO: Complete member initialization
            }
            
        }
        
    }
}
