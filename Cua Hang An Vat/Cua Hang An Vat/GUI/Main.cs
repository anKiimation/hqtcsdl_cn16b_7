﻿using Cua_Hang_An_Vat.BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.GUI
{
    public partial class Main : Form
    {
          BanAnController banAnController = new BanAnController();

        public Main()
        {
            InitializeComponent();
        }

     


        private void cÔngCụToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {

            string maSo = txtBanSo.Text;
            banAnController.traBan(maSo);
        }

        private void panelBanAn_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Main_Load(object sender, EventArgs e)
        {
            //gan Control
            banAnController.Panel = panelBanAn;
            banAnController.TxtMaSo = txtBanSo;
            banAnController.BtnGoiMon = btnGoiMon;
            banAnController.BtnThanhToan = btnThanhToan;
            banAnController.TxtThoiGianVaoBan = txtThoiGianVaoBan;
            banAnController.TxtTongSoMonAn = txtTongSoMonAn;
            banAnController.TxtKhuyenMai = txtKhuyenMai;
            banAnController.TxtTongTien = txtTongTien;

            //
            banAnController.loadBanAn();

        }

        private void btnGoiMon_Click(object sender, EventArgs e)
        {
            string maSo = txtBanSo.Text;
            if (!maSo.Equals(""))
            {
                banAnController.ngoiVaoBan(maSo);
                (new FormGoiMon()).Show();
            }
            else
            {
                MessageBox.Show("Chưa chọn bàn!", "Óc!");
            }
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new FormMenu()).Show();
        }

        private void tàiKhoảnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new FormQuanLyTaiKhoan()).Show();
        }

        private void mởMáyTínhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }
    }
}
