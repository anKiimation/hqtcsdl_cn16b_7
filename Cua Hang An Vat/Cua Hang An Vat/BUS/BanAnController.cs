﻿using Cua_Hang_An_Vat.DAL;
using Cua_Hang_An_Vat.DTO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.BUS
{
    class BanAnController
    {

        SQLBanAn sqlBanAn = new SQLBanAn();
        TextBox txtMaSo, txtThoiGianVaoBan, txtTongSoMonAn, txtKhuyenMai, txtTongTien;

       
       FlowLayoutPanel panel;
       Button btnGoiMon, btnThanhToan;

       public Button BtnThanhToan
       {
           get { return btnThanhToan; }
           set { btnThanhToan = value; }
       }

       public TextBox TxtThoiGianVaoBan
       {
           get { return txtThoiGianVaoBan; }
           set { txtThoiGianVaoBan = value; }
       }
       public Button BtnGoiMon
       {
           get { return btnGoiMon; }
           set { btnGoiMon = value; }
       }

       public FlowLayoutPanel Panel
       {
           get { return panel; }
           set { panel = value; }
       }

       public TextBox TxtMaSo
       {
           get { return txtMaSo; }
           set { txtMaSo = value; }
       }

  

       public TextBox TxtTongSoMonAn
       {
           get { return txtTongSoMonAn; }
           set { txtTongSoMonAn = value; }
       }

       public TextBox TxtKhuyenMai
       {
           get { return txtKhuyenMai; }
           set { txtKhuyenMai = value; }
       }

       public TextBox TxtTongTien
       {
           get { return txtTongTien; }
           set { txtTongTien = value; }
       }
        






        public void loadBanAn()
        {

            panel.Controls.Clear();
            List<BanAn> lstBanAn = new List<BanAn>();
              lstBanAn =  sqlBanAn.loadListBanAn();

            foreach (BanAn i in lstBanAn)
            {
                Button a = new Button();
                a.Tag = i;
                a.Text = i.MaSo;
                a.Font = new Font("Arial", 12);
                if (i.BanTrong)
                {
                    a.BackColor = Color.YellowGreen;
                }
                else
                    a.BackColor = Color.Tomato;



                a.Width = 85;
                a.Height = 85;
                a.Click += a_Click;
               

                Panel.Controls.Add(a);
            }


        }

        void a_Click(object sender, EventArgs e)
        {
            bool banTrong = ((BanAn)(((Button)sender).Tag)).BanTrong;
            String maSo = ((BanAn)(((Button)sender).Tag)).MaSo;
            if (banTrong)
            {
                btnThanhToan.Enabled = false;
            }
            else
            {
                btnThanhToan.Enabled = true;

            }
                hienThongTinBan(maSo);
            
      
        }


        public void ngoiVaoBan(String maSo)
        {
            
            sqlBanAn.ngoiVaoBan(maSo);
            hienThongTinBan(maSo);
            loadBanAn();

        }

        public void traBan(String maSo)
        {

            sqlBanAn.traBan(maSo);
            resetTextBox();
            loadBanAn();

        }

        public void hienThongTinBan(String maSo)
        {
            //  TextBox txtMaSo, txtGioVao, txtTongSoMonAn, txtKhuyenMai, txtTongTien;
            txtMaSo.Text = maSo;

            
                DateTime thoiGianVaoBan = DateTime.Now;
                int tongSoMonAn = 0;
                long khuyenMai = 0;
                long tongTien = 1;

                if (sqlBanAn.hienThiThongTinBanAn(ref maSo,ref  thoiGianVaoBan,ref tongSoMonAn,ref khuyenMai,ref tongTien))
                {


                    txtTongSoMonAn.Text = tongSoMonAn.ToString();
                    txtThoiGianVaoBan.Text = thoiGianVaoBan.ToString();
                    txtKhuyenMai.Text = khuyenMai.ToString();
                    txtTongTien.Text = tongTien.ToString();
                }
                else
                {
                    resetTextBox();
                }
            

        }

        private void resetTextBox()
        {
            txtTongSoMonAn.Text = "";
            txtThoiGianVaoBan.Text = "";
            txtKhuyenMai.Text = "";
            txtTongTien.Text = "";
        }

        

    }
}
