﻿using Cua_Hang_An_Vat.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cua_Hang_An_Vat.DAL
{
    class SQLBanAn
    {
        private SQLConnect sqlConnect = new SQLConnect();

        public List<BanAn> dataTableToList(DataTable data)
        {
            List<BanAn> lstTemp = new List<BanAn>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                String maSo = data.Rows[i]["MASO"].ToString();




                bool banTrong = Convert.ToBoolean(  data.Rows[i]["BANTRONG"].ToString());
                //if (banTrongTemp == 1)
                //    banTrong = true;


                BanAn a = new BanAn(maSo, banTrong);

                lstTemp.Add(a);

            }

            return lstTemp;
        }



        public List<BanAn> loadListBanAn()
        {
            String query = "SELECT * FROM BANAN";
            List<BanAn> lstBanAn = new List<BanAn>();
            lstBanAn  = this.dataTableToList(sqlConnect.executeQuery(query));
            return lstBanAn;
        }


        public void ngoiVaoBan(String maSo)
        {
            String query1 = "UPDATE BANAN SET BANTRONG = 0 WHERE MASO = '" + maSo + "'";
            String query2 = "INSERT INTO THONGTINBANAN(MASO) VALUES ('" + maSo + "')";
            sqlConnect.executeNonQuery(query1);
            sqlConnect.executeNonQuery(query2);
        }

        public void traBan(String maSo)
        {
            String query1 = "UPDATE BANAN SET BANTRONG = 1 WHERE MASO = '" + maSo + "'";
            String query2 = "DELETE FROM THONGTINBANAN WHERE MASO='" + maSo + "'";
            sqlConnect.executeNonQuery(query1);
            sqlConnect.executeNonQuery(query2);
        }

        public bool hienThiThongTinBanAn(ref String maSo, ref DateTime thoiGianVaoBan,ref  int tongSoMonAn, ref long khuyenMai, ref long TongTien)
        {
            String query = "SELECT * FROM THONGTINBANAN WHERE MASO = '" + maSo + "'";
            DataTable data = sqlConnect.executeQuery(query);
            try
            {
                thoiGianVaoBan = Convert.ToDateTime(data.Rows[0]["THOIGIANVAOBAN"].ToString());
                tongSoMonAn = Convert.ToInt32(data.Rows[0]["TONGSOMONAN"].ToString());
                khuyenMai = Convert.ToInt64(data.Rows[0]["khuyenmai"].ToString());
                TongTien = Convert.ToInt64(data.Rows[0]["tongtien"].ToString());
                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }

    }
}
