﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cua_Hang_An_Vat.DAL
{
    class SQLConnect
    {
        string strSQLConnect = "Data Source=DESKTOP-GO328FF\\SQLEXPRESS;Initial Catalog=QUANLYCUAHANGANVAT;Integrated Security=True";

        public string StrSQLConnect
        {
            get { return strSQLConnect; }
            set { strSQLConnect = value; }
        }

        public DataTable executeQuery(string query)
        {
            DataTable data = new DataTable();
            SqlConnection sql = new SqlConnection(strSQLConnect);
            sql.Open();
            SqlCommand command = new SqlCommand(query, sql);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            sql.Close();
            return data;
        }


        public void executeNonQuery(string query)
        {
            SqlConnection sql = new SqlConnection(strSQLConnect);
            sql.Open();
            SqlCommand command = new SqlCommand(query, sql);
            command.ExecuteNonQuery();
            sql.Close();
        }


        

    }
}


