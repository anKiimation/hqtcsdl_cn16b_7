﻿CREATE DATABASE QUANLYCUAHANGANVAT
GO
USE QUANLYCUAHANGANVAT 
GO

CREATE TABLE MONAN(
MASO VARCHAR(5) PRIMARY KEY,
TENMONAN NVARCHAR(50) NOT NULL,
GIATIEN BIGINT NOT NULL,
KHADUNG BIT NOT NULL DEFAULT 0,
)
GO




CREATE TABLE BANAN(
MASO VARCHAR(5) PRIMARY KEY,
BANTRONG BIT NOT NULL DEFAULT 1,
ANHIEN BIT  NOT NULL DEFAULT 1
)
GO



CREATE TABLE TAIKHOAN(
TENTAIKHOAN VARCHAR(32) PRIMARY KEY,
MATKHAU VARCHAR(32) NOT NULL,
TENNHANVIEN NVARCHAR(50) NOT NULL,
NGAYSINH DATE NOT NULL,
SODIENTHOAI VARCHAR(15) NOT NULL,
DIACHI NVARCHAR(100) NOT NULL,
ISADMIN BIT NOT NULL DEFAULT 0,
)


CREATE TABLE THONGTINBANAN(
MASO VARCHAR(5) FOREIGN KEY REFERENCES BANAN(MASO),
THOIGIANVAOBAN SMALLDATETIME NOT NULL DEFAULT GETDATE(),
TONGSOMONAN INT NOT NULL DEFAULT 0,

TONGTIEN BIGINT NOT NULL DEFAULT 0,
TENTAIKHOAN VARCHAR(32) FOREIGN KEY REFERENCES TAIKHOAN(TENTAIKHOAN),
CONSTRAINT MAHOADON PRIMARY KEY (MASO, THOIGIANVAOBAN)
)
GO
CREATE TABLE DANHSACHMONAN(

MABANAN VARCHAR(5) ,
THOIGIANVAOBAN SMALLDATETIME,
MAMONAN VARCHAR(5) FOREIGN KEY REFERENCES MONAN(MASO),
FOREIGN KEY (MABANAN, THOIGIANVAOBAN) REFERENCES THONGTINBANAN(MASO,THOIGIANVAOBAN),
MASOdanhsach INT IDENTITY PRIMARY KEY
)



go













INSERT INTO BANAN VALUES('BAN01',1,1)
INSERT INTO BANAN VALUES('BAN02',1,1)
INSERT INTO BANAN VALUES('BAN03',1,1)
INSERT INTO BANAN VALUES('BAN04',1,1)
INSERT INTO BANAN VALUES('BAN05',1,1)
INSERT INTO BANAN VALUES('BAN06',1,1)
INSERT INTO BANAN VALUES('BAN07',1,1)
INSERT INTO BANAN VALUES('BAN08',1,1)
INSERT INTO BANAN VALUES('BAN09',1,1)
INSERT INTO BANAN VALUES('BAN10',1,1)
INSERT INTO BANAN VALUES('BAN11',1,1)
INSERT INTO BANAN VALUES('BAN12',1,1)
INSERT INTO BANAN VALUES('BAN13',1,1)
INSERT INTO BANAN VALUES('BAN14',1,1)
INSERT INTO BANAN VALUES('BAN15',1,1)
INSERT INTO BANAN VALUES('BAN16',1,1)
INSERT INTO BANAN VALUES('BAN17',1,1)
INSERT INTO BANAN VALUES('BAN18',1,1)
INSERT INTO BANAN VALUES('BAN19',1,1)
INSERT INTO BANAN VALUES('BAN20',1,1)



GO
use QUANLYCUAHANGANVAT


GO 

insert into MONAN VALUES ('MON01', 'LAU THAI', 100000, 1);
insert into MONAN VALUES ('MON02', 'LAU GA', 50000, 1);
insert into MONAN VALUES ('MON03', 'LAU DE', 400000, 0);
insert into MONAN VALUES ('MON04', 'RAU MUONG XAO TOI', 900000, 1);
insert into MONAN VALUES ('MON05', 'TRAI CAY', 100000, 1);






--123456
insert into taikhoan values('lenguyenkhoa', 'E10ADC3949BA59ABBE56E057F20F883E', N'Lê Nguyên Khoa', '1998/08/01', '0357860936', 'PHÚ YÊN', 1)
insert into taikhoan values('lenguyenkhoa1', 'E10ADC3949BA59ABBE56E057F20F883E', N'Lê Nguyên Khoa Một', '1998/08/01', '04554554', 'Sài gòn',0)

USE QUANLYCUAHANGANVAT



--t-sql
--procEdure
go
CREATE PROCEDURE xemDanhSachMonAn
	@maBanAn varchar(5),
	@thoiGianVaoBan SMALLDATETIME
as
begin
	SELECT MASO, TENMONAN, GIATIEN, SOLUONG = COUNT(MASO) 
	FROM DANHSACHMONAN JOIN MONAN ON MAMONAN = MASO 
	WHERE MABANAN = @maBanAn 
	AND THOIGIANVAOBAN = @thoiGianVaoBan 
	GROUP BY MASO, TENMONAN,GIATIEN
end
go
--exec xemDanhSachMonAn @mabanan = 'ban10', @thoigianvaoban = '2019-05-17 15:22:00'

--function

go
create function tinhTongSoMonAn(@maBanAn varchar(5), @thoiGianVaoBan SMALLDATETIME)
returns integer
as
begin
return (select count(*) from DANHSACHMONAN where mabanan = @maBanAn and THOIGIANVAOBAN = @thoiGianVaoBan)
end

go
select tongSoMonAn, dbo.tinhTongSoMonAn(maso, THOIGIANVAOBAN) as xemtongso from THONGTINBANAN

go
create function tinhTongTien(@maBanAn varchar(5), @thoiGianVaoBan SMALLDATETIME)
returns integer
as
begin
declare @tongTien int
select @tongTien = sum(MONAN.giatien) from DANHSACHMONAN join MONAN on DANHSACHMONAN.MAMONAN = MONAN.MASO where mabanan = @maBanAn  and THOIGIANVAOBAN = @thoiGianVaoBan
return @tongTien
end

go
select TONGTIEN, dbo.tinhTongTien(maso, THOIGIANVAOBAN) as xemtongso from THONGTINBANAN

--trans
go
-- THEM MON AN 
CREATE PROCEDURE themMonAn @maBanAn varchar(5), @thoiGianVaoBan SMALLDATETIME, @maMonAn varchar(5)
as
begin
begin tran;
begin try

INSERT INTO DANHSACHMONAN VALUES(@maBanAn ,@thoiGianVaoBan ,@maMonAn ) --insert

update THONGTINBANAN --update
set TONGSOMONAN = dbo.tinhTongSoMonAn(@maBanAn,@thoiGianVaoBan),
tongtien = dbo.tinhTongTien(@maBanAn,@thoiGianVaoBan)
where  maso = @maBanAn and THOIGIANVAOBAN = @thoiGianVaoBan

IF @@Trancount > 0
     PRINT 'themMonAn thanh cong!'; -- thanh cong
		COMMIT TRAN;
end try
begin catch
PRINT 'Xay ra loi: ' + ERROR_MESSAGE();
 PRINT 'themMonAn That bai --> Rollback Transaction';
 IF @@Trancount > 0
   ROLLBACK TRAN;
end catch

end
go
--exec dbo.themMonAn @maBanAN = 'ban03', @thoiGianVaoBan = '2019-05-17 15:53:00', @maMonAn='mon01'

go
go
-- XOA MON AN
CREATE PROCEDURE xoaMonAn @maBanAn varchar(5), @thoiGianVaoBan SMALLDATETIME, @maMonAn varchar(5)
as
begin
begin tran;
begin try

DELETE top(1) FROM DANHSACHMONAN WHERE MABANAN = @maBanAn AND THOIGIANVAOBAN =@thoiGianVaoBan AND MAMONAN = @maMonAn --delete

update THONGTINBANAN --update
set TONGSOMONAN = dbo.tinhTongSoMonAn(@maBanAn,@thoiGianVaoBan),
tongtien = dbo.tinhTongTien(@maBanAn,@thoiGianVaoBan)
where  maso = @maBanAn and THOIGIANVAOBAN = @thoiGianVaoBan

IF @@Trancount > 0
     PRINT 'xoaMonAn thanh cong!'; -- thanh cong
		COMMIT TRAN;
end try
begin catch
PRINT 'Xay ra loi: ' + ERROR_MESSAGE();
 PRINT 'xoaMonAn That bai --> Rollback Transaction';
 IF @@Trancount > 0
   ROLLBACK TRAN;
end catch

end
go

--exec dbo.xoaMonAn @maBanAN = 'ban03', @thoiGianVaoBan = '2019-05-17 15:53:00', @maMonAn='mon01'

go

-- NGOI VAO BAN
CREATE PROCEDURE ngoiVaoBan @maBanAn varchar(5)
as
begin
begin tran;
begin try
UPDATE BANAN SET BANTRONG = 0 WHERE MASO = @maBanAn
INSERT INTO THONGTINBANAN(MASO) VALUES (@maBanAn)
IF @@Trancount > 0
     PRINT 'ngoiVaoBan thanh cong!'; -- thanh cong
		COMMIT TRAN;
end try
begin catch
PRINT 'Xay ra loi: ' + ERROR_MESSAGE();
 PRINT 'ngoiVaoBan That bai --> Rollback Transaction';
 IF @@Trancount > 0
   ROLLBACK TRAN;
end catch

end
go
--exec dbo.traBan @maBanAn = ''
-- TRA BAN
go
CREATE PROCEDURE traBan @maBanAn varchar(5), @thoiGianVaoBan SMALLDATETIME
as
begin
begin tran;
begin try
UPDATE BANAN SET BANTRONG = 1 WHERE MASO = @maBanAn
DELETE FROM THONGTINBANAN WHERE MASO = @maBanAn AND THOIGIANVAOBAN = @thoiGianVaoBan
IF @@Trancount > 0
     PRINT 'traBan thanh cong!'; -- thanh cong
		COMMIT TRAN;
end try
begin catch
PRINT 'Xay ra loi: ' + ERROR_MESSAGE();
 PRINT 'traBan That bai --> Rollback Transaction';
 IF @@Trancount > 0
   ROLLBACK TRAN;
end catch

end

-- TRA BAN THEM HOA DON
go
CREATE PROCEDURE traBanThemHoaDon @maBanAn varchar(5), @thoiGianVaoBan SMALLDATETIME, @tenTaiKhoan varchar(32)
as
begin
begin tran;
begin try
UPDATE BANAN SET BANTRONG = 1 WHERE MASO = @maBanAn
UPDATE THONGTINBANAN SET TENTAIKHOAN = @tenTaiKhoan WHERE MASO = @maBanAn AND THOIGIANVAOBAN = @thoiGianVaoBan
IF @@Trancount > 0
     PRINT 'traBan thanh cong!'; -- thanh cong
		COMMIT TRAN;
end try
begin catch
PRINT 'Xay ra loi: ' + ERROR_MESSAGE();
 PRINT 'traBan That bai --> Rollback Transaction';
 IF @@Trancount > 0
   ROLLBACK TRAN;
end catch

end

go
--exec dbo.traBan @maBanAn = '', @thoiGianVaoBan = ''

go

-- CHUYEN BAN
use QUANLYCUAHANGANVAT

go

CREATE PROCEDURE chuyenBan @maBanAn1 varchar(5),@thoiGianVaoBan smalldatetime, @maBanAn2 varchar(5)
as
begin
BEGIN TRAN;
BEGIN TRY
if(exists(select * from BANAN where BANTRONG = 1 and MASO = @maBanAn2 and ANHIEN = 1) )
begin
UPDATE BANAN SET BANTRONG = 1 WHERE MASO = @maBanAn1
UPDATE BANAN SET BANTRONG = 0 WHERE MASO = @maBanAn2

SELECT MABANAN,THOIGIANVAOBAN,MAMONAN INTO #danhSachMonAnTemp from DANHSACHMONAN where MABANAN = @maBanAn1 AND THOIGIANVAOBAN = @thoiGianVaoBan
delete from DANHSACHMONAN where MABANAN = @maBanAn1 AND THOIGIANVAOBAN = @thoiGianVaoBan

UPDATE THONGTINBANAN SET MASO = @maBanAn2, THOIGIANVAOBAN = GETDATE() WHERE MASO = @maBanAn1 AND THOIGIANVAOBAN = @thoiGianVaoBan

UPDATE #danhSachMonAnTemp SET MABANAN = @maBanAn2, THOIGIANVAOBAN =  GETDATE() WHERE MABANAN = @maBanAn1
INSERT INTO DANHSACHMONAN SELECT * FROM #danhSachMonAnTemp
drop table #danhSachMonAnTemp

 IF @@Trancount > 0
   COMMIT TRAN;
end
END TRY
begin catch

PRINT 'Xay ra loi: ' + ERROR_MESSAGE();
 PRINT 'CHUYEN BAN That bai --> Rollback Transaction';
 IF @@Trancount > 0
   ROLLBACK TRAN;

end catch
END


go



--exec chuyenBan @maBanAn1 = 'BAN17', @thoiGianVaoBan = '2019-05-20 02:21:00', @maBanAn2 = 'BAN01'

--TRIGGER


go
CREATE TRIGGER triggerAnHienBanAn
on banan
for update, insert
as
begin
if exists(select * from inserted where bantrong = 0 and anhien = 0)
begin
RAISERROR ('Bàn này đã có người ngồi', 16, 1)
rollback transaction
print ('Bàn này đã có người ngồi')
end
end














-- TRANG THAI CUA HANG
use QUANLYCUAHANGANVAT
GO
-- BAN AN DOANH THU THEO NGAY
SELECT MASO, SUM(TONGTIEN) AS TONGTIEN FROM THONGTINBANAN 
WHERE DAY(THOIGIANVAOBAN) = 21  and MONTH(THOIGIANVAOBAN) = 5 and year(thoigianvaoban) = 2019 
GROUP BY MASO 
ORDER BY TONGTIEN DESC

-- MON AN GOI NHIEU NHAT
SELECT TENMONAN, GIATIEN, COUNT(MAMONAN) AS SOLANGOI 
FROM MONAN JOIN DANHSACHMONAN 
ON DANHSACHMONAN.MAMONAN = MONAN.MASO 
where DAY(THOIGIANVAOBAN) = 21  and MONTH(THOIGIANVAOBAN) = 5 and year(thoigianvaoban) = 2019 
GROUP BY TENMONAN, GIATIEN 
ORDER BY SOLANGOI DESC

-- BAN AN NGOI NHIEU NHAT
SELECT maso,COUNT(MASO) AS SOLANNGOI 
FROM  THONGTINBANAN 
where   MONTH(THONGTINBANAN.THOIGIANVAOBAN) = 5 and year(THONGTINBANAN.THOIGIANVAOBAN) = 2019 
GROUP BY MASO 
ORDER BY SOLANNGOI DESC

-- TONG DOANH THU THEO NGAY
SELECT SUM(TONGTIEN) AS TONGDOANHTHUTHEONGAY FROM THONGTINBANAN 
WHERE DAY(THOIGIANVAOBAN) = 21 and MONTH(THOIGIANVAOBAN) = 5

-- TONG DOANH THU THEO THANG
SELECT SUM(TONGTIEN) AS 'Tổng doanh thu theo năm' FROM THONGTINBANAN 
WHERE MONTH(THOIGIANVAOBAN) = 5 and year(thoigianvaoban) = 2019

--load nam
SELECT distinct year(thoigianvaoban) FROM THONGTINBANAN

